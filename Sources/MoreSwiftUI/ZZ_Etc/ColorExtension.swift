import SwiftUI

public extension Color {
    init(hex: UInt32) {
        self.init(
            red:       Double((hex >> 16) & 0xFF) / 256.0,
            green:     Double((hex >> 8) & 0xFF) / 256.0,
            blue:      Double(hex & 0xFF) / 256.0
        )
    }
    
    init(rgbaHex: UInt32) {
        self.init(
            red:      Double((rgbaHex >> 24) & 0xFF) / 256.0,
            green:    Double((rgbaHex >> 16) & 0xFF) / 256.0,
            blue:     Double((rgbaHex >> 8) & 0xFF) / 256.0,
            opacity:  Double(rgbaHex & 0xFF) / 256.0
        )
    }
}

#if os(iOS)
public extension UIColor {
    convenience init(hex: UInt32) {
        self.init(
            _colorLiteralRed: Float((hex >> 24) & 0xFF) / 256.0,
            green:            Float((hex >> 16) & 0xFF) / 256.0,
            blue:             Float((hex >> 8) & 0xFF) / 256.0,
            alpha:            0
        )
    }
    
    convenience init(rgbaHex: UInt32) {
        self.init(
            _colorLiteralRed: Float((rgbaHex >> 24) & 0xFF) / 256.0,
            green:            Float((rgbaHex >> 16) & 0xFF) / 256.0,
            blue:             Float((rgbaHex >> 8) & 0xFF) / 256.0,
            alpha:            Float(rgbaHex & 0xFF) / 256.0
        )
    }
}
#else
public extension NSColor {
    convenience init(hex: UInt32) {
        self.init(
            _colorLiteralRed: Float((hex >> 24) & 0xFF) / 256.0,
            green:            Float((hex >> 16) & 0xFF) / 256.0,
            blue:             Float((hex >> 8) & 0xFF) / 256.0,
            alpha:            0
        )
    }
    
    convenience init(rgbaHex: UInt32) {
        self.init(
            _colorLiteralRed: Float((rgbaHex >> 24) & 0xFF) / 256.0,
            green:            Float((rgbaHex >> 16) & 0xFF) / 256.0,
            blue:             Float((rgbaHex >> 8) & 0xFF) / 256.0,
            alpha:            Float(rgbaHex & 0xFF) / 256.0
        )
    }
}
#endif

public extension Color {
    static var clickableAlpha: Color { get { return Color(rgbaHex: 0x01010101) } }
}

public extension View {
    ///Give ability to click / tap / hover/ etc on full space of View. Even on empty space.
    func makeFullyIntaractable() -> some View {
        self.background(Color.clickableAlpha)
    }
    
    ///Give ability to click / tap / hover/ etc on full space of View. Even on empty space.
    func makeFullyIntaractableOverlay() -> some View {
        self.overlay(Color.clickableAlpha)
    }
}

#if os(macOS)


@available(macOS 12.0, *)
public extension Color {
    var nsColor: NSColor { NSColor(self) }
    var cgColor: CGColor { nsColor.cgColor }
}

@available(macOS 12.0, *)
public func dynamicColor(light: Color, dark: Color) -> Color {
    Color(nsColor: NSColor(light: light.nsColor, dark: dark.nsColor) )
}


@available(macOS 11.0, *)
public extension NSColor {
    convenience init(light: NSColor, dark: NSColor) {
        self.init(name: nil, dynamicProvider: { $0.isDarkMode ? dark : light } )
    }
    
    var inverted: NSColor {
        var r: CGFloat = 0.0, g: CGFloat = 0.0, b: CGFloat = 0.0, a: CGFloat = 0.0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        return NSColor(red: (1 - r), green: (1 - g), blue: (1 - b), alpha: a)
    }
    
    @available(macOS 12.0, *)
    var color: SwiftUI.Color {
        Color(nsColor: self)
    }
}

internal extension NSAppearance {
    @inlinable var isDarkMode: Bool {
        if #available(macOS 10.14, *), self.bestMatch(from: [.darkAqua, .aqua]) == .darkAqua {
            return true
        }
        return false
    }
}
#endif

#if os(iOS)

public extension UIColor {
    @available(macOS 12.0, *)
    var color: SwiftUI.Color {
        Color(uiColor: self)
    }
}
#endif
