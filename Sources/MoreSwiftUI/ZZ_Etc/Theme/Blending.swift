import SwiftUI

extension ColorScheme {
    func white(`for` blender: Blender) -> Color {
        switch self {
        case .light:        return Blender.BURN().white(for: blender)
        case .dark:         return Blender.DODGE().white(for: blender)
        @unknown default:   return Blender.BURN().white(for: blender)
        }
    }
    
    var blendMode : BlendMode {
        switch self {
        case .light:
            return .colorBurn
        case .dark:
            return .colorDodge
        @unknown default:
            return .colorBurn
        }
    }
}

public protocol BlenderInstance {
    var values : [Double] { get }
    var mode : BlendMode { get }
}

extension BlenderInstance {
    func white(`for`: Blender) -> Color {
        return Color.init(white: values[`for`.rawValue])
    }
}


public enum Blender : Int {
    case zero   = 0
    case lvl_1  = 1
    case lvl_2  = 2
    case lvl_3  = 3
    case lvl_4  = 4
    case lvl_5  = 5
    case lvl_6  = 6
    case lvl_7  = 7
    case lvl_8  = 8
    case lvl_9  = 9
    case one    = 10
    
    public func decrement(for dec: Int?) -> Blender {
        if let dec = dec {
            let raw = max(self.rawValue - dec, 0)
            return Blender(rawValue: raw) ?? self
        }
        return self
    }
}

@available(macOS 10.15, *)
public extension Blender {
    struct BURN : BlenderInstance {
        public let values: [Double] = [1.0, 0.75, 0.5, 0.44, 0.33, 0.25, 0.19, 0.14, 0.11, 0.08, 0.0]
//        public let values: [Double] = [1.0, 0.7, 0.45, 0.33, 0.25, 0.19, 0.14, 0.11, 0.08, 0.04, 0.0]
        public var mode : BlendMode { .colorBurn }
        
        public init() {}
    }
    
    struct DODGE : BlenderInstance {
        public let values: [Double] = [0.0, 0.20, 0.36, 0.49, 0.59, 0.67, 0.74, 0.79, 0.83, 0.86, 1.0]
        public var mode : BlendMode { .colorDodge }
        
        public init() {}
    }
}
