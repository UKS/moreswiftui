#if os(macOS)
import AppKit

public extension NSScrollView {
    func setContentOffset(_ newOffset: NSPoint, animated: Bool) {
        if animated {
            let animationDuration: Double = 0.3
            NSAnimationContext.beginGrouping()
            NSAnimationContext.current.duration = animationDuration
            let clipView = self.contentView
            clipView.animator().setBoundsOrigin(newOffset)
            self.reflectScrolledClipView(self.contentView)
            NSAnimationContext.endGrouping()
            
            return
        }
        
        self.contentView.bounds.origin = newOffset
    }
    
    var contentOffset: CGPoint {
        get { self.contentView.bounds.origin }
        set { self.contentView.bounds.origin = newValue }
    }
    
    var contentSizeFixed: CGSize {
        CGSize(width: self.contentView.bounds.maxX, height: self.contentView.bounds.maxY)
    }
}
#endif
