
import Foundation
import SwiftUI

public enum SheetDialogType {
    case none
    case view(view: AnyView )
}

public extension SheetDialogType {
    var isNone: Bool {
        if case .none = self {
            return true
        }
        return false
    }
    
    var isShown: Bool {
        if case .none = self {
            return false
        }
        return true
    }
    
    @ViewBuilder
    func asView() -> some View  {
        switch self {
        case .none:             EmptyView()
        case .view(let view):   view
        }
    }
}

@available(OSX 12.0, *)
public extension View {
    func sheet(sheet: SheetDialogType) -> some View {
        self.modifier( CustomSheetMod(sheetDialogType: .constant(sheet) ) )
    }
    
    func sheet(sheet: Binding<SheetDialogType>) -> some View {
        self.modifier( CustomSheetMod(sheetDialogType: sheet ) )
    }
    
    @ViewBuilder
    func sheet2(sheet: SheetDialogType) -> some View {
        if sheet.isShown {
            ZStack {
                self
                
                VisualEffectView()
                
                sheet.asView()
            }
        } else {
            self
        }
    }
}

@available(OSX 12.0, *)
struct CustomSheetMod: ViewModifier {
    @Binding var sheetDialogType: SheetDialogType
    @Environment(\.colorScheme) var scheme
    
    public func body (content: Content) -> some View {
        content
            #if os(macOS)
            .blur(radius: sheetDialogType.isShown ? 1.4 : 0)
            #endif
            .sheet(isPresented: .constant(sheetDialogType.isShown), onDismiss: { sheetDialogType = .none } ) {
                sheetDialogType
                    .asView()
                    .background {
                        if scheme == .dark {
                            Rectangle()
                                .fill(Color.gray)
                                .opacity(0.1575)
                        }
                        if scheme == .light {
                            Rectangle()
                                .fill(Color.black)
                                .opacity(0.075)
                        }
                    }
                
            }
    }
}
