#if os(macOS)

import Foundation
import SwiftUI

extension View {
    public func cursor(_ cursor: NSCursor) -> some View {
        self.onHover { inside in
            DispatchQueue.main.async {
                if inside {
                    cursor.push()
                } else {
                    NSCursor.pop()
                }
            }
        }
    }
    
    public func cursorPointHand() -> some View {
        self.cursor(.pointingHand)
    }
}

#endif
