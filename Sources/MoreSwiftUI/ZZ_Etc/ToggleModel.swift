 ////////////////////
 // Ability to use array of BOOLs as set of toggle values source
 ///////////////////

public struct ToggleModel<T>: Hashable where T: Hashable {
    public static func == (lhs: ToggleModel<T>, rhs: ToggleModel<T>) -> Bool {
        lhs.hashValue == rhs.hashValue
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(item)
    }
    
    public let item: T
    public var checked: Bool = true
    
    /// Ability to use array of BOOLs as set of toggle values source
    ///```
    ///@State private var questModels = ["q1","q2","q3"].map{ ToggleModel($0) }
    ////////////
    ///ForEach(questModels.indices) { idx in
    ///    HStack {
    ///        Toggle("", isOn: .constant(self.questModels[idx].checked))
    ///
    ///        Text(self.questModels[idx].item)
    ///    }
    ///    .overlay(Color.clickableAlpha)
    ///    .onTapGesture { self.questModels[idx].checked.toggle() }
    ///}
    ///```
    public init(_ item: T, checked: Bool) {
        self.item = item
        self.checked = checked
    }
}
