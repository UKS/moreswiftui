import SwiftUI
import Combine
#if os(iOS)
import UIKit
#endif
import Essentials

@available(macOS 10.15, *)
@available(iOS 13, *)
fileprivate var cancellables = [String : AnyCancellable] ()

@available(macOS 10.15, *)
@available(iOS 13, *)
public extension Published {
    
    /// Correct way to use:
    ///```
    /// @Published(key: "isLogedIn") var isLogedIn = false
    ///```
    //URL
    init(wrappedValue defaultValue: Value, key: String) where Value == URL {
        let value = (UserDefaults.standard.object(forKey: key) as? String)
                        .flatMap { $0.asURL() } ?? defaultValue
        
        // init wrapped variable
        self.init(initialValue: value)
        
        // Save to UserDefaults
        cancellables[key] = projectedValue.sink { val in
            UserDefaults.standard.set(val.path , forKey: key)
        }
    }
    
    //Color
    init(wrappedValue defaultValue: Value, key: String) where Value == Color {
        let value = (UserDefaults.standard.object(forKey: key) as? String)
                        .flatMap { UnifiedColor(hexString: $0) }
                        .flatMap { Color( $0) } ?? defaultValue
        
        // init wrapped variable
        self.init(initialValue: value)
        
        // Save to UserDefaults
        cancellables[key] = projectedValue.sink { val in
            UserDefaults.standard.set(UnifiedColor(val).hexString , forKey: key)
        }
    }

    
    //Array<RawRepresentable> where RawRepresentable == String
    init<T: RawRepresentable>(wrappedValue defaultValue: Value, key: String) where Value == Array<T> , T.RawValue == String {
        
        let value = ( UserDefaults.standard.object(forKey: key) as? String )
            .flatMap {
                $0.split(separator: "|")
                    .map { "\($0)" }
                    .map { T(rawValue: $0) }
                    .compactMap{ $0 } as Value
            } ?? defaultValue
        
        // init wrapped variable
        self.init(initialValue: value )
        
        // Save to UserDefaults
        cancellables[key] = projectedValue.sink { val in
            let joinedStr = defaultValue.map{ $0.rawValue }.joined(separator: "|")
            
            UserDefaults.standard.set(joinedStr, forKey: key)
        }
    }
    
    //RawRepresentable
    init(wrappedValue defaultValue: Value, key: String) where Value: RawRepresentable, Value.RawValue : Equatable {
        let value = ( UserDefaults.standard.object(forKey: key) as? Value.RawValue )
                        .flatMap { Value(rawValue: $0) } ?? defaultValue
        
        // init wrapped variable
        self.init(initialValue: value)
        
        // Save to UserDefaults
        cancellables[key] = projectedValue.sink { val in
            UserDefaults.standard.set(val.rawValue, forKey: key)
        }
    }
    
    init(wrappedValue defaultValue: Value, key: String) {
        let value = UserDefaults.standard.object(forKey: key) as? Value ?? defaultValue
        
        // init wrapped variable
        self.init(initialValue: value)
        
        // Save to UserDefaults
        cancellables[key] = projectedValue.sink { val in
            UserDefaults.standard.set(val, forKey: key)
        }
    }
}

////////////////////////////
///HELPERS
////////////////////////////
#if os(macOS)
    fileprivate typealias UnifiedColor = NSColor
#endif
    
#if os(iOS)
    fileprivate typealias UnifiedColor = UIColor
#endif

extension URL: RawRepresentable {
    public typealias RawValue = String
    
    public var rawValue: String { self.path }
    
    public init?(rawValue: RawValue) {
        self = URL(fileURLWithPath: rawValue)
    }
}
