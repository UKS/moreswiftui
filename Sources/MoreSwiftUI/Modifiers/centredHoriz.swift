import Foundation
import SwiftUI

public extension View {
    func centredHoriz() -> some View {
        HStack {
            Spacer()
            self
            Spacer()
        }
    }
    
    func centredVert() -> some View {
        VStack {
            Spacer()
            self
            Spacer()
        }
    }
}
