import Foundation
import SwiftUI

public enum Side: Equatable, Hashable {
    case left
    case right
    case top
    case bottom
    case all
}

public extension View {
    /// Altirnative to standard .padding modifier
    ///
    /// This padding always will be shown and never be less than you set. Not like standard padding system.
    @ViewBuilder
    func paddingAlt(_ sides: [Side], value: CGFloat = 8) -> some View {
        if !sides.contains(.bottom) && !sides.contains(.top) && !sides.contains(.left) && !sides.contains(.right) {
            self
        } else if !sides.contains(.bottom) && !sides.contains(.top) {
            hPadding(sides: sides, value: value)
        } else if !sides.contains(.left) && !sides.contains(.right) {
            vPadding(sides: sides, value: value)
        } else {
            allSidePadding(sides: sides, value: value)
        }
    }
}

fileprivate extension View {
    private func hPadding(sides: [Side], value: CGFloat) -> some View {
        HStack(spacing: 0) {
            if sides.contains(.left) {
                Space(value, .h)
            }
            
            self
            
            if sides.contains(.right) {
                Space(value, .h)
            }
        }
    }
    
    private func vPadding(sides: [Side], value: CGFloat) -> some View {
        VStack(spacing: 0) {
            if sides.contains(.top) {
                Space(value, .v)
            }
            
            self
            
            if sides.contains(.bottom) {
                Space(value, .v)
            }
        }
    }
    
    private func allSidePadding(sides: [Side], value: CGFloat) -> some View {
        VStack(spacing: 0) {
            if sides.contains(.all) || sides.contains(.top) {
                Space(value, .v)
            }
            
            HStack(spacing: 0) {
                if sides.contains(.all) || sides.contains(.left) {
                    Space(value, .h)
                }
                
                self
                
                if sides.contains(.all) || sides.contains(.right) {
                    Space(value, .h)
                }
            }
            
            if sides.contains(.all) || sides.contains(.bottom) {
                Space(value, .v)
            }
        }
    }
}
