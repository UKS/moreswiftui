import SwiftUI

public extension View {
    /// IMPORTANT! Better to use standard SwiftUI's if else system!
    ///```
    ///Text("some Text")
    ///   .if(modifierEnabled) { $0.foregroundColor(.red) }
    ///```
    @ViewBuilder
    func `if`<Content: View>(_ condition: Bool, @ViewBuilder content: (Self) -> Content) -> some View {
        if condition {
            content(self)
        } else {
            self
        }
    }
}

public extension View {
    /// IMPORTANT! Better to use standard SwiftUI's if else system!
    ///```
    /// Text("some Text")
    ///     .if(modifierEnabled) { $0.foregroundColor(.Red) }
    ///     else:                { $0.background(Color.Blue) }
    ///```
    @ViewBuilder
    func `if`<TrueContent: View, FalseContent: View>
              (_ condition: Bool,
               @ViewBuilder ifTrue trueContent: (Self) -> TrueContent,
               @ViewBuilder else  falseContent: (Self) -> FalseContent ) -> some View
    {
        if condition {
            trueContent(self)
        } else {
            falseContent(self)
        }
    }
}
