import SwiftUI
#if os(macOS)
@available(OSX 11.0, *)
public extension View {
    ///```
    ///.sidebar(shown: $sidebarShown) {
    ///    ScrollView(.vertical) {
    ///        VStack {
    ///            Text("hello world")
    ///        }
    ///        .padding(.horizontal, 20)
    ///        .padding(.vertical, 10)
    ///    }
    ///    .backgroundGaussianBlur(type: .withinWindow, material: .m1_hudWindow)
    ///}
    ///```
    func sidebar<Content2>(shown: Binding<Bool>, content: @escaping () -> Content2) -> some View where Content2: View {
        self.modifier(SidebarModifier(shown, content: content))
    }
}

@available(OSX 11.0, *)
public struct SidebarModifier<Content2>: ViewModifier where Content2: View {
    @Binding var shown: Bool
    let sidebar: () -> Content2
    
    public init(_ shown: Binding<Bool>, content: @escaping () -> Content2) {
        _shown = shown
        self.sidebar = content
    }
    
    public func body(content: Content) -> some View {
        ZStack {
            content
            
            HStack {
                if shown {
                    Button(action: { withAnimation{ shown = false } }) {
                        Spacer()
                            .fillParent()
                            .makeFullyIntaractable()
                    }
                    .buttonStyle(.plain)
                    
                    sidebar()
                        .transition( .move(edge: .trailing) )
                        .animation( .easeOut(duration: 0.1), value: shown)
                }
            }
        }
    }
}
#endif
