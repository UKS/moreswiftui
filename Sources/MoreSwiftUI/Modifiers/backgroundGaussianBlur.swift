#if os(macOS)

import Foundation
import SwiftUI

@available(OSX 11.0, *)
public extension View {
    func backgroundGaussianBlur(type: NSVisualEffectView.BlendingMode = .withinWindow, material: GausianMaterial = .m1_hudWindow) -> some View {
        self.background( VisualEffectView(type: type, material: material) )
    }
}

@available(OSX 12.0, *)
public extension View {
    func backgroundGaussianBlur(type: NSVisualEffectView.BlendingMode = .withinWindow, material: GausianMaterial = .m1_hudWindow, color: Color? = nil) -> some View {
        self.background {
            if let color = color {
                ZStack {
                    VisualEffectView(type: type, material: material)
                    
                    color
                }
            } else {
                VisualEffectView(type: type, material: material)
            }
        }
    }
}

#endif
