import Foundation
import SwiftUI

#if os(macOS)

public extension View {
    func dragWndWithClick() -> some View {
        self.overlay(DragWndView())
    }
}

public struct DragWndView: View {
    public let test: Bool
    
    public init(test: Bool = false) {
        self.test = test
    }
    
    public var body: some View {
        ( test ? Color.green : Color.clickableAlpha )
            .overlay( DragWndNSRepr() )
    }
}

///////////////
///HELPERS
///////////////
fileprivate struct DragWndNSRepr: NSViewRepresentable {
    func makeNSView(context: Context) -> NSView {
        return DragWndNSView()
    }
    
    func updateNSView(_ nsView: NSView, context: Context) { }
}

fileprivate class DragWndNSView: NSView {
    override public func mouseDown(with event: NSEvent) {
        window?.performDrag(with: event)
    }
}

#endif
