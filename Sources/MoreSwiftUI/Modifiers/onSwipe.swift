import SwiftUI
import Combine
#if os(macOS)
public extension View {
    ///
    ///```
    /// .onSwipe { event in
    ///    switch event.direction {
    ///    case .up:
    ///        print("up")
    ///    case .down:
    ///        print("down")
    ///    case .left:
    ///        print("left")
    ///    case .right:
    ///        print("right")
    ///    default:
    ///        print("nothing")
    ///    }
    /// }
    ///```
    @available(OSX 11.0, *)
    func onSwipe(perform action: @escaping (SwipeEvent) -> Void) -> some View {
        modifier(OnSwipe(action: action))
    }
}

@available(OSX 11.0, *)
public class SwipeEvent {
    public enum SwipeDirection {
        case none, up, down, left, right
    }
    
    public enum Modifier {
        case none, shift, control, option, command
    }
    
    public enum Compass {
        case none, north, south, west, east, northWest, southWest, northEast, southEast
    }
    
    var nsevent: NSEvent! = nil
    
    var directionValue: CGFloat = .zero
    var phase: NSEvent.Phase = .mayBegin
    
    var deltaX: CGFloat = .zero
    var deltaY: CGFloat = .zero
    var location: CGPoint = .zero
    var timestamp: TimeInterval = .nan
    var mouseLocation: CGPoint = .zero
    var scrollingDeltaX: CGFloat = .zero
    var scrollingDeltaY: CGFloat = .zero
    var modifierFlags: NSEvent.ModifierFlags = .shift
    
    public init(event: NSEvent) {
        nsevent = event
        guard nsevent.window != nil else { return }
        
        //----- copy the nsevent data
        scrollingDeltaX = nsevent.scrollingDeltaX
        scrollingDeltaY = nsevent.scrollingDeltaY
        phase = nsevent.phase
        deltaX = nsevent.deltaX
        deltaY = nsevent.deltaY
        scrollingDeltaX = nsevent.scrollingDeltaX
        scrollingDeltaY = nsevent.scrollingDeltaY
        location = nsevent.locationInWindow
        mouseLocation = nsevent.locationInWindow
        location = nsevent.cgEvent!.location
        timestamp = nsevent.timestamp
    }
    
    public var direction: SwipeDirection {
        if nsevent.scrollingDeltaX > 0.0 { return .left  }
        if nsevent.scrollingDeltaX < 0.0 { return .right }
        if nsevent.scrollingDeltaY > 0.0 { return .up    }
        if nsevent.scrollingDeltaY < 0.0 { return .down  }
        
        return .none
    }
    
    public var compass: Compass {
        var directionEastWest: Compass = .none
        var directionNorthSouth: Compass = .none
        
        if nsevent.scrollingDeltaX > 0.0 { directionEastWest   = .east  }
        if nsevent.scrollingDeltaX < 0.0 { directionEastWest   = .west  }
        if nsevent.scrollingDeltaY > 0.0 { directionNorthSouth = .north }
        if nsevent.scrollingDeltaY < 0.0 { directionNorthSouth = .south }
        
        if nsevent.scrollingDeltaY == 0 { return directionEastWest   }
        if nsevent.scrollingDeltaX == 0 { return directionNorthSouth }
        
        if directionNorthSouth == .north && directionEastWest == .east { return .northEast }
        if directionNorthSouth == .south && directionEastWest == .east { return .southEast }
        if directionNorthSouth == .north && directionEastWest == .west { return .northWest }
        if directionNorthSouth == .south && directionEastWest == .west { return .southWest }
        
        return .none
    }
    
    public var modifier: Modifier {
        if nsevent.modifierFlags.contains(.shift)   { return .shift   }
        if nsevent.modifierFlags.contains(.control) { return .control }
        if nsevent.modifierFlags.contains(.option ) { return .option  }
        if nsevent.modifierFlags.contains(.command) { return .command }
        
        return  .none
    }
}

private struct OnSwipe: ViewModifier {
    var minTimeDistance = 0.6
    var action: (SwipeEvent) -> Void
    
    @State private var insideViewWindow = false
    
    @State private var swipeEvent = SwipeEvent(event: NSEvent())
    
    @State var subs = Set<AnyCancellable>() // Cancel onDisappear
    
    @State var lastUse: Date = Date()
    
    func body(content: Content) -> some View {
        return content
            .checkThat(viewInsideWindow: $insideViewWindow)
            .onAppear {
                NSApp.publisher(for: \.currentEvent)
                    .filter { event in event?.type == .scrollWheel }
                    .filter { event in event?.phase == .began }
                    .map { $0! }
                    .sink { event in
                        let timeDistance = lastUse.distance(to: Date())
                        
                        if insideViewWindow
                            && timeDistance > minTimeDistance
                            && ( abs(event.scrollingDeltaX) >= 1.0 || abs(event.scrollingDeltaY) >= 1.0 )
                        {
                            action( SwipeEvent(event: event) )
                        }
                        
                        lastUse = Date()
                    }
                    .store(in: &subs)
            }
            .onDisappear {
                subs.removeAll()
            }
    }
}

#elseif os(iOS)
public extension View {
    /// Adds a swipe gesture to the view.
    /// Usage Example:
    /// ```
    /// .onSwipe { event in
    ///     switch event.direction {
    ///     case .up:
    ///         print("up")
    ///     case .down:
    ///         print("down")
    ///     case .left:
    ///         print("left")
    ///     case .right:
    ///         print("right")
    ///     default:
    ///         print("nothing")
    ///     }
    /// }
    /// ```
    func onSwipe(perform action: @escaping (SwipeEvent) -> Void) -> some View {
        modifier(OnSwipe(action: action))
    }
}

public struct SwipeEvent {
    public enum SwipeDirection {
        case none, up, down, left, right
    }
    
    public var direction: SwipeDirection
    
    public init(direction: SwipeDirection) {
        self.direction = direction
    }
}

private struct OnSwipe: ViewModifier {
    var action: (SwipeEvent) -> Void
    
    func body(content: Content) -> some View {
        content
            .gesture(
                DragGesture()
                    .onEnded { value in
                        let horizontalAmount = value.translation.width
                        let verticalAmount = value.translation.height
                        
                        let direction: SwipeEvent.SwipeDirection
                        if abs(horizontalAmount) > abs(verticalAmount) {
                            // Horizontal swipe (swapping directions)
                            direction = horizontalAmount > 0 ? .right : .left
                        } else {
                            // Vertical swipe
                            direction = verticalAmount > 0 ? .down : .up
                        }
                        
                        action(SwipeEvent(direction: direction))
                    }
            )
    }
}
#endif

extension View {
    func checkThat(viewInsideWindow: Binding<Bool>) -> some View {
        self.modify {
            if #available(macOS 14, * ) {
                $0.onContinuousHover { phase in
                    switch phase {
                    case .active:
                        viewInsideWindow.wrappedValue = true
                        
                    case .ended:
                        viewInsideWindow.wrappedValue = false
                    }
                }
            } else {
                $0.onHover {
                    viewInsideWindow.wrappedValue = $0
                }
            }
        }
    }
}
