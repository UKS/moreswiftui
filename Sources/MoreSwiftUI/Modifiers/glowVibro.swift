import Foundation
import SwiftUI

@available(macOS 12.0, *)
public extension View {
    func glowVibro(radius: CGFloat = 10, duration: CGFloat = 2) -> some View {
        self.modifier(GlowModifier(radius: radius, duration: duration))
    }
}

@available(macOS 12.0, *)
public struct GlowModifier: ViewModifier {
    var radius: CGFloat
    var duration: CGFloat
    
    @State private var flag = true
    
    var animation: Animation {
        [
            Animation.easeIn(duration: duration),
            Animation.easeInOut(duration: duration),
            Animation.easeOut(duration: duration),
            Animation.linear(duration: duration),
        ]
        .randomElement()!
        .repeatForever()
    }
    
    public func body(content: Content) -> some View {
        content
            .background {
                content
                    .animationMovementDisable()
                    .opacity(flag ? 1 : 0.7 )
                    .blur(radius: flag ? radius : radius * 0.7)
                    .onAppear {
                        flag.toggle()
                    }
                    .animation(animation, value: flag)
            }
    }
}
