import SwiftUI


public extension View {
    @ViewBuilder func maskReverse<Mask: View>(
        alignment: Alignment = .center,
        @ViewBuilder _ mask: () -> Mask
    ) -> some View {
        if #available(macOS 12.0, *) {
            self.mask {
                Rectangle()
                    .overlay(alignment: alignment) {
                        mask()
                            .blendMode(.destinationOut)
                    }
            }
        } else {
            // Fallback on earlier versions
        }
    }
}
