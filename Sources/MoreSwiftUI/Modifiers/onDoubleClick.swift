#if os(macOS)

import Foundation
import SwiftUI

@available(OSX 12, *)
public extension View {
    func onDoubleClick(handler: @escaping () -> Void) -> some View {
        modifier(DoubleClickHandler(handler: handler))
    }
}

@available(OSX 12, *)
public struct DoubleClickHandler: ViewModifier {
    let handler: () -> Void
    public func body(content: Content) -> some View {
        content.overlay {
            DoubleClickListeningViewRepresentable(handler: handler)
        }
    }
}

@available(OSX 12, *)
public struct DoubleClickListeningViewRepresentable: NSViewRepresentable {
    let handler: () -> Void
    
    public func makeNSView(context: Context) -> DoubleClickListeningView {
        DoubleClickListeningView(handler: handler)
    }
    
    public func updateNSView(_ nsView: DoubleClickListeningView, context: Context) {}
}

@available(OSX 12, *)
public class DoubleClickListeningView: NSView {
    let handler: () -> Void
    
    public init(handler: @escaping () -> Void) {
        self.handler = handler
        super.init(frame: .zero)
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func mouseDown(with event: NSEvent) {
        if event.clickCount == 2 {
            handler()
        }
        super.mouseDown(with: event)
    }
}

#endif
