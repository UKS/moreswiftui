import SwiftUI

public extension View {
    func modify(@ViewBuilder _ modifier: (Self) -> some View) -> some View {
        return modifier(self)
    }
}
