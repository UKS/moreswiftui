#if os(macOS)
import Foundation
import SwiftUI

@available(macOS 11.0, *)
public extension View {
    
    /// * CAN BE CAUSE OF MEMORY LEAKS because of usage WndAccessor inside
    /// * Reaction on keyboard's .keyDown will be executed only in case of view located at window where window.isKeyWindow == true
    /// * But reaction will be even if this window displays .sheet
    /// * KeyCode struct located in Essentials
    /// * Active Keyboard Layout does not matter
    /// ```
    ///.keyboardReaction { event in
    ///    switch event.keyCode {
    ///    case KeyCode.escape:
    ///        print("esc pressed!")
    ///        return nil // disable beep sound
    ///    case KeyCode.a:
    ///        print("A pressed!")
    ///        return nil // disable beep sound
    ///    default:
    ///        break
    ///    }
    ///    return event // beep sound will be here
    ///}
    /// ```
    @available(*, deprecated, message: "use keyboardReaction2() instead this!!!!")
    func keyboardReaction(action: @escaping (NSEvent) -> (NSEvent?) ) -> some View {
        self.modifier(KeyboardReactiomModifier(action: action))
    }
    
    /// * WORKS WITH ALL OF WINDOWS AT THE SAME TIME
    /// * Reaction on keyboard's .keyDown will be executed only in case of view located at window where window.isKeyWindow == true
    /// * But reaction will be even if this window displays .sheet
    /// * KeyCode struct located in Essentials
    /// * Active Keyboard Layout does not matter
    /// ```
    ///.keyboardReaction2 { event in
    ///    switch event.keyCode {
    ///    case KeyCode.escape:
    ///        print("esc pressed!")
    ///        return nil // disable beep sound
    ///    case KeyCode.a:
    ///        print("A pressed!")
    ///        return nil // disable beep sound
    ///    default:
    ///        break
    ///    }
    ///    return event // beep sound will be here
    ///}
    func keyboardReaction2(action: @escaping (NSEvent) -> (NSEvent?) ) -> some View {
        self.modifier(KeyboardReactiomModifier2(action: action))
    }
}

@available(macOS 11.0, *)
@available(*, deprecated, message: "use keyboardReaction2() instead this!!!!")
private struct KeyboardReactiomModifier: ViewModifier {
    let action: (NSEvent) -> (NSEvent?)
    
    @State var window: NSWindow? = nil
    
    func body(content: Content) -> some View {
        content
            .wndAccessor { self.window = $0 }
            .onAppear {
                onAppearReact()
            }
    }
    
    func onAppearReact() {
        NSEvent.addLocalMonitorForEvents(matching: .keyDown) { event in
            guard let window = window,
                  window.isKeyWindow
            else { return event }
            
            return action(event)
        }
    }
}


@available(macOS 11.0, *)
private struct KeyboardReactiomModifier2: ViewModifier {
    let action: (NSEvent) -> (NSEvent?)
    
    @State var keyMonitor: Any?
    
    func body(content: Content) -> some View {
        content
            .onAppear {
                keyMonitor = NSEvent.addLocalMonitorForEvents(matching: [.keyDown], handler: action)
            }
            .onDisappear {
                if keyMonitor != nil {
                    NSEvent.removeMonitor(keyMonitor!)
                    keyMonitor = nil
                }
            }
    }
}
#endif
