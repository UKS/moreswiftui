import Foundation
import SwiftUI

@available(OSX 11.0, *)
@available(iOS 13.4, *)
public extension View {
    func dragAs(string: String, addSpaceBeginEnd: Bool = true) -> some View  {
        self.onDrag { return NSItemProvider(object: String("\(addSpaceBeginEnd ? " " : "")\(string)\(addSpaceBeginEnd ? " " : "")") as NSString ) }
            #if os(macOS)
            .cursor(.openHand)
            #endif
    }
}
