import Foundation
import SwiftUI

public extension View {
    func onScenePhaseChange(phase: ScenePhase, action: @escaping () -> ()) -> some View {
        self.modifier( OnScenePhaseChangeModifier(phase: phase, action: action) )
    }
}

public struct OnScenePhaseChangeModifier: ViewModifier {
    @Environment(\.scenePhase) private var scenePhase
    
    public let phase: ScenePhase
    
    public let action: () -> ()
    
    public func body(content: Content) -> some View {
        content
            .onChange(of: scenePhase) { phase in
                if (phase == phase) {
                    action()
                }
            }
    }
}
