import Foundation
import SwiftUI

public extension Text {
    func letterSpacing(_ tracking: CGFloat) -> Text {
        self.tracking(tracking)
    }
}
