import SwiftUI

public extension View {
    @available(*, deprecated, message: "Use .animatiomMovementDisable() instead")
    func animationDisable() -> some View {
        self.animation(nil, value: UUID())
    }
    
    func animationMovementDisable() -> some View  {
        self
            .transaction { transaction in
                transaction.animation = nil
            }
    }
}
