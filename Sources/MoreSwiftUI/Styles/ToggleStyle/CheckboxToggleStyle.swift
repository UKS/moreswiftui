#if os(iOS)

import Foundation
import SwiftUI

public extension ToggleStyle where Self == CheckboxToggleStyle {
    static var checkboxCust: CheckboxToggleStyle { .init() }
}

public struct CheckboxToggleStyle: ToggleStyle {
    public func makeBody(configuration: Configuration) -> some View {
       HStack {
           RoundedRectangle(cornerRadius: 5.0)
               .stroke(lineWidth: 2)
               .frame(width: 25, height: 25)
               .cornerRadius(5.0)
               .overlay {
                   if configuration.isOn {
                       Image(systemName: "checkmark")
                   }
               }
               .onTapGesture {
                   withAnimation(.easeInOut(duration: 0.1)) {
                       configuration.isOn.toggle()
                   }
               }
           
           configuration.label
       }
   }
}

#endif
