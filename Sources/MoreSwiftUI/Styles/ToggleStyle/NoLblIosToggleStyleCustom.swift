import SwiftUI

public extension ToggleStyle where Self == NoLblIosToggleStyleCustom {
    static var nolblIosStyle: NoLblIosToggleStyleCustom { NoLblIosToggleStyleCustom(width: 29, height: 15, cornerRadius: 20, color: .green) }
}

public struct NoLblIosToggleStyleCustom: ToggleStyle {
    var width: CGFloat
    var height: CGFloat
    var cornerRadius: CGFloat
    var color: Color
    
    @State var isHovering = false
    
    public init(width: CGFloat = 40, height: CGFloat = 22, cornerRadius: CGFloat = 20, color: Color = .green) {
        self.width = width
        self.height = height
        self.cornerRadius = cornerRadius
        self.color = color
    }
    
    public func makeBody(configuration: Configuration) -> some View {
        HStack {
//            configuration.label
//            Space(min:0)
            Rectangle()
                .foregroundColor(configuration.isOn ? color : .gray)
                .frame(width: width, height: height, alignment: .center)
                .overlay(
                    Circle()
                        .foregroundColor(.white)
                        .padding(.all, 1)
                        .offset(x: configuration.isOn ? width/4.5 : -(width/4.5), y: 0)
                        .animation(Animation.linear(duration: 0.1), value: configuration.isOn)
                )
                .cornerRadius(20)
                .onTapGesture { configuration.isOn.toggle() }
                .onHover { hvr in withAnimation { isHovering = hvr } }
                .opacity ( isHovering ? 1 : 0.9 )
        }
    }
}
