import Foundation
import SwiftUI

public struct BtnUksStyle { }

public extension BtnUksStyle {
    static var `default` : BtnUksStylesSet.Fancy { BtnUksStylesSet.Fancy()  }
}

public struct BtnUksStylesSet {
    public struct Fancy: ButtonStyle {
        @State var scale : CGFloat
        @State var hover: Bool = false
        
        public init(scale: CGFloat = 1) {
            self.scale = scale
        }
        
        public func makeBody(configuration: Self.Configuration) -> some View {
            configuration.label
                .scaleEffect(scale)
                .onChange(of: configuration.isPressed) { newValue in
                    if (!configuration.isPressed) {
                        withAnimation(.spring(dampingFraction: 0.5).speed(2)) {
                            scale = 0.95
                        }
                    } else {
                        withAnimation(.spring(dampingFraction: 0.5).speed(2)) {
                            scale = 1
                        }
                    }
                }
                .onHover { hover in
                    withAnimation(.easeInOut(duration: 0.5)) {
                        self.hover = hover
                    }
                }
                .opacity(hover ? 1 : 0.8)
        }
    }
    
    @available(macOS 12.0, *)
    public struct Highlight: ButtonStyle {
        @State var scale : CGFloat = 1
        @State var hover: Bool = false
        @State var color: Color
        
        public init(color: Color = .accentColor) {
            self.color = color
        }
        
        public func makeBody(configuration: Self.Configuration) -> some View {
            configuration.label
                .scaleEffect(scale)
                .onChange(of: configuration.isPressed) { newValue in
                    if (!configuration.isPressed) {
                        withAnimation(.spring(dampingFraction: 0.5).speed(2)) {
                            scale = 1
                        }
                    } else {
                        withAnimation(.spring(dampingFraction: 0.5).speed(2)) {
                            scale = 0.95
                        }
                    }
                }
                .onHover { hover in
                    withAnimation(.easeInOut(duration: 0.5)) {
                        self.hover = hover
                    }
                }
                .background {
                    if hover {
                        RoundedRectangle(cornerRadius: 5)
                            .fill(color)
                    }
                }
        }
    }
}

