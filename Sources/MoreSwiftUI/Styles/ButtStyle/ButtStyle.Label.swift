
import Foundation
import SwiftUI

#if os(macOS)

public extension ButtStyle {
    struct Label: ButtonStyle {
        let color : Color?
        let dec : Int?
        
        public init(color: Color? = nil, decrement: Int? = nil) {
            self.color = color
            self.dec = decrement
        }
        
        public func makeBody(configuration: Configuration) -> some View {
            LabelButtView(configuration: configuration, color: color, dec: dec)
        }
    }
}

private struct LabelButtView : View {
    @Environment(\.isEnabled) var isEnabled: Bool
    let configuration: ButtonStyle.Configuration
    let color : Color?
    let dec : Int?
    
    @State var hover : Bool = false
    
    var blender : Blender {
        if isEnabled {
            return hover ? (configuration.isPressed ? .lvl_9 : .lvl_8) : .lvl_7
        } else {
            return .lvl_4
        }
    }
    
    var body: some View {
        ZStack {
            configuration.label
                .theme(.normal(blender.decrement(for: dec)))
                .animation(.default, value: hover)
                .animation(.default, value: configuration.isPressed)
            
            if let color = color {
                configuration.label
                    .foregroundColor(color)
                    .opacity(0.3)
            }
                
        }.onHover { hover = $0 }
    }
}

#endif
