import Foundation
import SwiftUI

public struct ToggStyle {
    
}

extension ToggStyle {
    public struct Slider: ToggleStyle {
        let color: Color
        let height: CGFloat
        public init(color: Color = .green, height: CGFloat = 20) {
            self.color = color
            self.height = height
        }
        
        public func makeBody(configuration: Configuration) -> some View {
            HStack {
                configuration.label
                Space(10)
                
                ZStack {
                    Rectangle()
                        .theme(.normal(.lvl_4))
                        .cornerRadius(20)
                    
                    if configuration.isOn {
                        Rectangle()
                            .foregroundColor(color)
                            .opacity(0.7)
                            .cornerRadius(20)
                    }
                }
                .overlay(
                    Circle()
                        .foregroundColor(.white)
                        .padding(.all, 3)
                        .overlay(
                            Image(systemName: configuration.isOn ? "checkmark" : "xmark")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .font(Font.title.weight(.black))
                                .frame(width: 8, height: 8, alignment: .center)
                                .foregroundColor(configuration.isOn ? color : .gray)
                        )
                        .offset(x: configuration.isOn ? 11 : -11, y: 0)
                        .animation(Animation.linear(duration: 0.1), value: configuration.isOn)
                )
                .frame(width: 48, height: height)
                .onTapGesture { configuration.isOn.toggle() }
            }
        }
    }
}
