#if os(macOS)

import Foundation
import SwiftUI

public extension ButtStyle.PopOver {
    enum Direction {
        case up
        case down
    }
    
    struct Label: ButtonStyle {
        let color : Color?
        let dec : Int?
        let direction : Direction
        
        public init(color: Color? = nil, decrement: Int? = nil, direction: Direction = .down) {
            self.color = color
            self.dec = decrement
            self.direction = direction
        }
        
        public func makeBody(configuration: Configuration) -> some View {
            PopOverLabelButtView(configuration: configuration, color: color, dec: dec, direction: direction)
        }
    }
}

private struct PopOverLabelButtView : View {
    @Environment(\.isEnabled) var isEnabled: Bool
    let configuration: ButtonStyle.Configuration
    let color : Color?
    let dec : Int?
    let direction : ButtStyle.PopOver.Direction
    
    @State var hover : Bool = false
    
    var blender : Blender {
        if isEnabled {
            return hover ? (configuration.isPressed ? .lvl_9 : .lvl_8) : .lvl_7
        } else {
            return .lvl_4
        }
    }
    
    var body: some View {
        HStack(spacing: 0) {
            ZStack {
                configuration.label
                    .theme(.normal(blender.decrement(for: dec)))
                    .animation(.default, value: hover)
                    .animation(.default, value: configuration.isPressed)
                
                if let color = color {
                    configuration.label
                        .foregroundColor(color)
                        .opacity(0.3)
                }
                
            }
            
            Text("^").fontWeight(.ultraLight)
                .rotationEffect(.degrees(direction == .down ? 180 : 0))
                .opacity(hover ? 1 : 0.5)
                .padding(.leading, 2)
                .padding(.trailing, 3)
                .padding(.bottom, 2)
        }.onHover { hover = $0 }
    }
}

#endif
