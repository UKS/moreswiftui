
import Foundation
import SwiftUI

@available(macOS 12.0, *)
extension ToggStyle {
    public struct Tao: ToggleStyle {
        
        public init() {
            
        }
        
        public func makeBody(configuration: Configuration) -> some View {
            TaoToggleImpl(configuration: configuration)
        }
    }
}

@available(macOS 12.0, *)
fileprivate struct TaoToggleImpl : View {
    let configuration : ToggleStyleConfiguration
    
    var body: some View {
        HStack(alignment: .center) {
            Button(action: { configuration.isOn.toggle() }) {
                    ZStack {
                        Image(systemName: "checkmark")
                            .resizable()
                            .scaledToFit()
                            .theme(.normal(.lvl_6))
                            .opacity(configuration.isOn ? 1 : 0)
                    }
                    .frame(height: 7)
                    .padding(5)
                    .background {
                        RoundedRectangle(cornerRadius: 4)
                            .stroke()
                            .theme(.normal(.lvl_3))
                    }
                
            }
            .buttonStyle(ButtStyle.RectTheme(cornerRadius: 4))
            .offset(y: 1)
            
            configuration.label
        }
    }
}

@available(macOS 12.0, *)
struct Toggle_PreviewsView : View {
    @State var isOn = true
    
    var body : some View {
        Toggle(isOn: $isOn) {
            Text("Hello Toggle")
        }
        .toggleStyle(ToggStyle.Tao())
    }
}
