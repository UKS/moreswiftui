import Foundation
import SwiftUI

@available(macOS 12.0, *)
public extension ButtStyle {
    struct RectTheme: ButtonStyle {
        let cornerRadius : CGFloat
        
        public init(cornerRadius: CGFloat = 10) {
            self.cornerRadius = cornerRadius
        }
        
        public func makeBody(configuration: Configuration) -> some View {
            RectThemeButtView(configuration: configuration, cornerRadius: cornerRadius)
        }
    }
}

@available(macOS 12.0, *)
fileprivate struct RectThemeButtView : View {
    @Environment(\.isEnabled) var isEnabled: Bool
    let configuration: ButtonStyle.Configuration
    let cornerRadius : CGFloat
    @State var hover : Bool = false
    
    var themeBlender : Blender {
        guard isEnabled else { return .lvl_1 }
        if hover {
            return configuration.isPressed ? .lvl_4 : .lvl_3
        }
        return configuration.isPressed ? .lvl_3 : .lvl_2
    }
    
    var body: some View {

        configuration.label
            .opacity(isEnabled ? 1 : 0.5)
            .background {
                RoundedRectangle(cornerRadius: cornerRadius)
                    .theme(.normal(themeBlender))
            }
            .animation(.default, value: isEnabled)
            .animation(.default, value: configuration.isPressed)
            .animation(.default, value: hover)
            .onHover { hover = $0 }
    }
}
