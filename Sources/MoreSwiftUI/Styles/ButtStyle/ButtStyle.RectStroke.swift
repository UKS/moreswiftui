import Foundation
import SwiftUI

public extension ButtStyle {
    struct RectStroke: ButtonStyle {
        let padding : CGFloat
        
        public init(padding : CGFloat = 1) {
            self.padding = padding
        }
        
        public func makeBody(configuration: Configuration) -> some View {
            RectHoverButtView(configuration: configuration, padding: padding)
        }
    }
}

struct RectHoverButtView : View {
    let configuration: ButtonStyle.Configuration
    let padding : CGFloat
    
    @State var hover : Bool = false
    
    var body: some View {
        ZStack {
            configuration.label
                .padding(padding)
                .background(
                    RoundedRectangle(cornerRadius: 7).stroke().theme(.normal(.lvl_5))
                        //.padding(.horizontal, -3)
                        .opacity(hover ? 1 : 0)
                )
        }
        .onHover    { hover = $0 }
        .animation(.default, value: hover)
    }
}
