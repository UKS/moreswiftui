
import Foundation
import SwiftUI

public extension ButtStyle {
    struct RectSolid: ButtonStyle {
        let padding : CGFloat
        let color : Color
        
        public init(padding: CGFloat = 10, color: Color = Color(hex: 0x00901f)) {
            self.padding = padding
            self.color   = color
        }
        
        public func makeBody(configuration: Configuration) -> some View {
            RectSolidButtView(configuration: configuration, padding: padding, color: color)
        }
    }
}

struct RectSolidButtView : View {
    @Environment(\.isEnabled) var isEnabled: Bool
    let configuration: ButtonStyle.Configuration
    let padding : CGFloat
    let color: Color
    @State var hover : Bool = false
    
    var body: some View {
        ZStack {
            configuration.label
                .opacity(isEnabled ? 1 : 0.5)
                .padding(padding)
                .background(BG(hover: hover, conf: configuration, color: color))
        }
        .onHover { hover = $0 }
        .animation(.default, value: isEnabled)
        .animation(.default, value: hover)
        .animation(.default, value: configuration.isPressed)
    }
}

fileprivate struct BG : View {
    @Environment(\.isEnabled) var isEnabled: Bool
    let hover : Bool
    let conf: ButtonStyle.Configuration
    let color : Color
    
    var body: some View {
        if isEnabled {
            RoundedRectangle(cornerRadius: 7)
                .fill(color)
                .opacity(conf.isPressed ? 0.6 : (hover ? 1 : 0.75))
        } else {
            RoundedRectangle(cornerRadius: 7)
                .fill(.gray)
                .opacity(0.5)
        }
    }
}

