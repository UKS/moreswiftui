#if os(macOS)

import Foundation
import SwiftUI

@available(macOS 12.0, *)
public extension ButtStyle {
    struct Icon: ButtonStyle {
        let id : String
        let color : Color?
        
        public init(id: String, color: Color? = nil) {
            self.id = id
            self.color = color
        }
        
        public func makeBody(configuration: Configuration) -> some View {
            IconButtonView(id: id, configuration: configuration, color: color)
        }
    }
}

@available(macOS 12.0, *)
struct IconButtonView : View {
    let id : String
    let configuration: ButtonStyle.Configuration
    let color : Color?
    
    @State var hover : Bool = false
    
    var body: some View {
        ZStack {
            if let color = color {
                Image(systemName: id)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(color)
                    .opacity(hover ? (configuration.isPressed ? 0.7 : 1.0) : 0.8)
            } else {
                Image(systemName: id)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .theme(.normal(hover ? (configuration.isPressed ? .lvl_7 : .lvl_8) : .lvl_5))
                    .animation(.default, value: hover)
                    .animation(.default, value: configuration.isPressed)
            }
                
        }
        .onHover { hover = $0 }
        .animation(.default, value: hover)
        .animation(.default, value: configuration.isPressed)
        .background {
            configuration.label
        }
    }
}

#endif
