import Foundation
import SwiftUI

public struct ButtStyle : ButtonStyle {
    let shapes: ButtStyle.Shapes
    let modifiers : ButtStyle.Modifiers
    
    public init(shapes: ButtStyle.Shapes, modifiers: ButtStyle.Modifiers) {
        self.shapes = shapes
        self.modifiers = modifiers
    }
    
    public func makeBody(configuration: Self.Configuration) -> some View {
        ButtView(shapes: shapes, modifiers: modifiers, configuration: configuration)
    }
}

struct ButtView : View {
    @Environment(\.isEnabled) var isEnabled: Bool
    @State var hover : Bool = false
    
    let shapes: ButtStyle.Shapes
    let modifiers : ButtStyle.Modifiers
    let configuration: ButtonStyle.Configuration
    
    public var body: some View {
        configuration.label
            .background(background)
            .animation(.default, value: isEnabled)
            .animation(.default, value: hover)
            .animation(.default, value: configuration.isPressed)
            .onHover {
                hover = $0
            }
    }
    
    @ViewBuilder
    var background : some View {
        if isEnabled {
            if configuration.isPressed {
                shapes.pressed.asView.apply(modifiers.pressed)
            } else if hover {
                shapes.hover.asView.apply(modifiers.hover)
            } else {
                shapes.normal.asView.apply(modifiers.normal)
            }
        } else {
            shapes.normal.asView
        }
    }
}

extension View {
    func apply( _ mod: ButtStyle.Modifier) -> some View {
        self
            .if(mod.theme != nil)   { $0.theme(mod.theme!) }
            .if(mod.opacity != nil) { $0.opacity(mod.opacity!) }
            .if(mod.color != nil)   { $0.foregroundColor(mod.color)}
    }
}

public extension ButtStyle.Modifier {
    static func theme( _ theme: Theme)      -> Self { .init(opacity: nil,     theme: theme, color: nil) }
    static func opacity( _ opacity: Double) -> Self { .init(opacity: opacity, theme: nil,   color: nil) }
    static func color( _ color: Color)      -> Self { .init(opacity: nil,     theme: nil,   color: color) }
}

public extension ButtStyle.Modifiers {
    func theme( _ normal: Theme, hover: Theme, press: Theme, disabled: Theme) -> Self {
        .init(normal: .theme(normal), hover: .theme(hover), pressed: .theme(press), disabled: .theme(disabled))
    }
}

public extension ButtStyle.Shape {
    @ViewBuilder
    var asView : some View {
        switch self {
        case .rect(rounded: let radius): RoundedRectangle(cornerRadius: radius)
        case .strokeRect(rounded: let radius): RoundedRectangle(cornerRadius: radius).stroke()
        }
    }
}

public extension ButtStyle.Shapes {
    static func rectRadius(normal: Double, hover: Double, pressed: Double) -> Self {
        .init(normal: .rect(rounded: normal), hover: .rect(rounded: hover), pressed: .rect(rounded: pressed))
    }
    static func rect(radius: Double) -> Self {
        .init(normal: .rect(rounded: radius), hover: .rect(rounded: radius), pressed: .rect(rounded: radius))
    }
    
    static func strokeRect(radius: Double) -> Self {
        .init(normal: .strokeRect(rounded: radius), hover: .strokeRect(rounded: radius), pressed: .strokeRect(rounded: radius))
    }
}

public extension ButtStyle {
    struct Modifiers {
        public let normal  : ButtStyle.Modifier
        public let hover   : ButtStyle.Modifier
        public let pressed : ButtStyle.Modifier
        public let disabled: ButtStyle.Modifier
        
        public init(normal: ButtStyle.Modifier, hover: ButtStyle.Modifier, pressed: ButtStyle.Modifier, disabled: ButtStyle.Modifier) {
            self.normal = normal
            self.hover = hover
            self.pressed = pressed
            self.disabled = disabled
        }
    }
    
    struct Modifier {
        public let opacity: Double?
        public let theme: Theme?
        public let color: Color?
    }
    
    struct Shapes {
        let normal  : ButtStyle.Shape
        let hover   : ButtStyle.Shape
        let pressed : ButtStyle.Shape
    }
    
    enum Shape {
        case rect(rounded: CGFloat)
        case strokeRect(rounded: CGFloat)
    }
}
