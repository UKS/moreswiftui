#if os(macOS)

import Foundation
import SwiftUI

public extension ButtStyle {
    struct PopOver {
        public struct InBetween: ButtonStyle {
            let color : Color?
            let defaultAlpha : CGFloat
            
            public init(color: Color? = nil, defaultAlpha : CGFloat = 0.8) {
                self.color = color
                self.defaultAlpha = defaultAlpha
            }
            
            public func makeBody(configuration: Configuration) -> some View {
                PopOverButtonView(configuration: configuration, color: color, defaultAlpha: defaultAlpha)
            }
        }
    }
}

private struct PopOverButtonView : View {
    let configuration: ButtonStyle.Configuration
    let color : Color?
    let defaultAlpha : CGFloat
    @Environment(\.isEnabled) var isEnabled: Bool
    @State var hover : Bool = false
    
    var labelOpacity : CGFloat {
        if configuration.isPressed {
            return 0.7
        }
        return hover ? 1 : defaultAlpha
    }
    
    var body: some View {
        HStack(spacing: 0) {
            configuration.label
                .foregroundColor(color)
                .opacity(labelOpacity)
                .padding(.leading, 5)

            Text("^").fontWeight(.ultraLight)
                .rotationEffect(.degrees(180))
                .opacity(hover ? 1 : 0.5)
                .padding(.leading, 2)
                .padding(.trailing, 3)
                .padding(.bottom, 2)
                .foregroundColor(color)
        }
        .background(
            HStack {
                if let color = color {
                    Rectangle().frame(width: 1).foregroundColor(color)
                    Spacer()
                    Rectangle().frame(width: 1).foregroundColor(color)
                } else {
                    Rectangle().frame(width: 1).theme(.normal(.lvl_5))
                    Spacer()
                    Rectangle().frame(width: 1).theme(.normal(.lvl_5))
                }
            }.padding(.vertical, 3)
                .opacity(hover ? 1 : 0)
        )
        .onHover {
            if isEnabled {
                hover = $0
            }
        }
        .animation(.default, value: hover)
        .animation(.default, value: configuration.isPressed)
    }
}

#endif
