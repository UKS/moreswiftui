import Foundation
import SwiftUI

@available(macOS 12.0, *)
@available(iOS 16.0, *)
public extension ButtStyle {
    struct Tab: ButtonStyle {
        var isOn: Bool
        var isNext: Bool
        var isPop: Bool
        
        public init(isOn: Bool, isNext: Bool = false, isPop: Bool = false) {
            self.isOn = isOn
            self.isNext = isNext
            self.isPop  = isPop
        }
        
        public func makeBody(configuration: Self.Configuration) -> some View {
            TabButtonView(configuration: configuration, color: nil, isOn: isOn, isNext: isNext, isPop: isPop)
            
        }
    }
}

@available(macOS 12.0, *)
@available(iOS 16.0, *)
struct TabButtonView : View {
    let configuration: ButtonStyle.Configuration
    let color : Color?
    @Environment(\.isEnabled) var isEnabled: Bool
    @State var hover : Bool = false
    var isOn: Bool
    var isNext: Bool
    var isPop: Bool
    
    var opacity : CGFloat {
        if isOn {
            return 1.0
        } else {
            return hover ? 1.0 : 0.5
        }
    }
    
    public var body: some View {
        configuration.label
            .opacity(opacity)
            .background {
                if isPop && isOn {
                        HStack {
                            Rectangle().frame(width: 1).theme(.normal(.lvl_2))
                            Spacer()
                            Rectangle().frame(width: 1).theme(.normal(.lvl_2))
                        }
                        .padding(.horizontal, 5)
                        .padding(.vertical, 5)
                        .opacity(hover ? 1 : 0)
                }
                if isNext {
                    if hover {
                        EmptyView()
                    } else {
                        Rectangle()
                            .theme(.normal(.lvl_1))
                            .opacity(isOn ? 0.0 : 1.0)
                    }
                } else {
                    Rectangle()
                        .theme(
                            hover ? .normal(.lvl_1) : (isOn ? .normal(.lvl_1) : .normal(.lvl_2))
                        )
                        //.opacity(isOn ? 0.0 : 1.0)
                }
                
            }.onHover { self.hover = $0 }
            .animation(.default, value: self.hover)
    }
}
