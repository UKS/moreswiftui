
import SwiftUI

public extension Image {
    init(packageResource name: String, ofType type: String) {
        #if canImport(UIKit)
        guard let path = Bundle.main.path(forResource: name, ofType: type),
              let image = UIImage(contentsOfFile: path) else {
            self.init(name)
            return
        }
        self.init(uiImage: image)
        #elseif canImport(AppKit)
        guard let path = Bundle.main.path(forResource: name, ofType: type),
              let image = NSImage(contentsOfFile: path) else {
            self.init(name)
            return
        }
        self.init(nsImage: image)
        #else
        self.init(name)
        #endif
    }
    
    init(named: String) {
        #if canImport(UIKit)
        if let img = UIImage(named: named) {
            self.init(uiImage: img)
        } else {
            self.init(named)
        }
        #elseif canImport(AppKit)
        if let img = NSImage(named: named) {
            self.init(nsImage: img)
        } else {
            self.init(named)
        }
        #else
        self.init(name)
        #endif
    }
}
