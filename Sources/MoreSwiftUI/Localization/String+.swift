import Foundation
import SwiftUI

#if DEBUG
// nil = system locale
// "En" = english
// "Uk" = ukrainian
// "Ge" = german
public var forceCurrentLocale: String?
#else
public var forceCurrentLocale: String?
#endif

public extension String {
    var localized: String {
        return localizedString(for: self, locale: chooseLocale(nil) )
    }
    
    func localized(_ params: CVarArg...) -> String {
        localized(locale: nil, params: params)
    }
    
    func localized(locale: Locale?, params: CVarArg...) -> String {
        let key: String = self
        let locale: Locale = chooseLocale(locale)
        
        let currentLocaleValue = localizedString(for: key, locale: locale, params: params)
        
        //if we have no translation for this language, we return Eng
        if currentLocaleValue == key {
            let engLocaleValue = localizedString(for: key, locale: Locale(identifier: "en"), params: params)
            
            return params.count > 0
                    ? String.localizedStringWithFormat(engLocaleValue, params)
                    : engLocaleValue
        }
        
        // in another case return valuse for curr lang
        return currentLocaleValue
    }
}

func localizedString(for key: String, locale: Locale = .current, params: CVarArg...) -> String {
    if #available(macOS 13, *) {
        guard let language = locale.language.languageCode?.identifier,
              let path = Bundle.main.path(forResource: language, ofType: "lproj") ?? Bundle.main.path(forResource: "en", ofType: "lproj")
        else {
            let error = "LOCALIZTION ERROR: \(locale.identifier), key:\(key)"
            //        printDbg(error)
            return error
        }
        
        let bundle = Bundle(path: path)!
        let localizedStr = NSLocalizedString(key, bundle: bundle, comment: "")
        let formattedLocalizedStr = String(format: localizedStr, params)
        
        return formattedLocalizedStr
            .replacingOccurrences(of: "\\n", with: "\n")
    } else {
        let language = locale.languageCode
        guard let path = Bundle.main.path(forResource: language, ofType: "lproj") ?? Bundle.main.path(forResource: "en", ofType: "lproj") else {
            let error = "LOCALIZTION ERROR: \(locale.identifier), key:\(key)"
    //        printDbg(error)
            return error
        }
        
        let bundle = Bundle(path: path)!
        let localizedStr = NSLocalizedString(key, bundle: bundle, comment: "")
        let formattedLocalizedStr = String(format: localizedStr, params)
    
        return formattedLocalizedStr
            .replacingOccurrences(of: "\\n", with: "\n")
    }
}

func chooseLocale(_ locale: Locale?) -> Locale {
    if let locale {
        return locale
    }
    
    if let forceCurrentLocale {
        return Locale.init(identifier: forceCurrentLocale)
    } else {
        return Locale.current
    }
}
