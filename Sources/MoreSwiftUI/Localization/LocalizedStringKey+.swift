import Foundation
import SwiftUI
import Essentials

//////////////////////////
///HELPERS
/////////////////////////

@available(macOS 10.15, *)
@available(iOS 15, *)
public extension LocalizedStringKey {
    var localized: String { localizedStr() }
    var localizedEn: String { localizedStr(locale: Locale(identifier: "En")) }
    
    func localizedStr(_ args: [CVarArg]? = nil, locale: Locale? = nil ) -> String {
        //use reflection
        let mirror = Mirror(reflecting: self)
        
        let currLocale = chooseLocale(locale)
        
        //try to find 'key' attribute value
        let key = mirror.children.first { (arg0) -> Bool in
            let (label, _) = arg0
            
            return label == "key"
        }?.value as? String
        
        //ask for localization of found key via NSLocalizedString
        if let key {
            let currentLocaleValue = localizedString(for: key, locale: currLocale )
            
            if currentLocaleValue == key { // this means we have no translation for this language
                //                let engLocaleValue = localizedEn  FOR UKS
                let engLocaleValue = localizedString(for: key, locale: Locale(identifier: "En"))
                
                if let args = args {
                    let a = String.localizedStringWithFormat(engLocaleValue, args )
                    
                    return a
                }
                
                return engLocaleValue.replacingOccurrences(of: "\\n", with: "\n")
            }
            
            if let args = args {
                let a = String.localizedStringWithFormat(currentLocaleValue, args )
                
                return a
            }
            
            return currentLocaleValue.replacingOccurrences(of: "\\n", with: "\n")
        }
        
        return "Swift LocalizedStringKey signature must have changed. @see Apple documentation."
    }
}
