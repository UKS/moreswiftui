
import Foundation

public extension Locale {
    static var en: Locale { Locale(identifier: "en") }
    static var uk: Locale { Locale(identifier: "uk") }
    static var ru: Locale { Locale(identifier: "ru") }
    static var ge: Locale { Locale(identifier: "ge") }
}
