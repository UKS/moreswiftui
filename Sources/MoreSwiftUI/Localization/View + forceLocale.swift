import SwiftUI

public extension View {
    @ViewBuilder
    func forceLocale(_ locale: String? ) -> some View {
        if let locale = locale,
            Locale.current.identifier != Locale(identifier: locale).identifier
        {
            self.environment(\.locale, Locale(identifier: locale))
        } else {
            self
        }
    }
}
