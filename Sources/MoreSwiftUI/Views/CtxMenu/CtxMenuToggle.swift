import Foundation
import SwiftUI

public struct CtxMenuToggle: View {
    let lbl: LocalizedStringKey
    @Binding var checked: Bool
    
    public init(_ lbl: LocalizedStringKey, checked: Binding<Bool>) {
        self.lbl = lbl
        _checked = checked
    }
    
    public var body: some View {
        Button(action: { checked.toggle() } ) {
            HStack(spacing: 0) {
                if checked {
                    Image(systemName: "checkmark")
                    Text(lbl)
                } else {
                    Text("      " + lbl.localizedStr() )
                }
            }
        }
    }
}


public struct CtxMenuToggle2: View {
    let lbl: LocalizedStringKey
    let checked: Bool
    let action: () -> ()
    
    public init(_ lbl: LocalizedStringKey, checked: Bool, action: @escaping () -> ()) {
        self.lbl = lbl
        self.checked = checked
        self.action = action
    }
    
    public var body: some View {
        Button(action: action ) {
            HStack(spacing: 0) {
                if checked {
                    Image(systemName: "checkmark")
                    Text(lbl)
                } else {
                    Text("      " + lbl.localizedStr() )
                }
            }
        }
    }
}
