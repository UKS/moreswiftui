import SwiftUI

@available(iOS 16.4, *)
@available(OSX 11.0.0, *)
public struct ToggleMixed : View {
    public var state : CheckState = CheckState.mixed
    public var action : () -> Void = {}
    
    public var width: CGFloat
    public var height: CGFloat
    
    @State private var rotating: Bool = false
    
    public init( state: CheckState,width: CGFloat = 18, height: CGFloat = 18, action: @escaping () -> Void ){
        self.state  = state
        self.action = action
        self.width = width
        self.height = height
    }
    
    public var body : some View {
#if os(macOS)
        macOSView()
#elseif os(iOS)
        iOSView()
#endif
    }
    
    func macOSView() -> some View {
        ZStack {
            Rectangle().theme(.normal(.lvl_2))
            
            Text(state.str).foregroundColor(.white)
        }
        .frame(width: width, height: height)
        .cornerRadius(5)
        .onTapGesture {
            self.action()
        }
        .animation(.easeInOut(duration: 0.3), value: state)
    }
    
    #if os(iOS)
    func iOSView() -> some View {
        RoundedRectangle(cornerRadius: 20)
            .fill(state.color)
            .animation(.linear(duration: 0.3), value: state)
            .frame(width: width, height: height, alignment: .center)
            .overlay(
                RoundedRectangle(cornerRadius: state.cornerRadius)
                    .fill(.white)
                    .rotationEffect(state.rotationAngle)
                    .animation(.linear(duration: 0.3), value: state)
                    .frame(width: height/state.rectangleSize, height: height/state.rectangleSize)
                    .padding(.all, 1)
                    .offset(x: width/state.isOn, y: 0)
            )
            .onTapGesture {
                self.action()
            }
    }
    #endif
    
}

@available(iOS 16.4, *)
@available(OSX 11.0.0, *)
public extension ToggleMixed {
    enum CheckState {
        case on
        case off
        case mixed
        
        public var str : String {
            switch self {
            case .on:       return "✓"
            case .off:      return " "
            case .mixed:    return "■"
            }
        }
        
        public mutating func toggle() {
            switch self {
            case .on:    self = .off
            case .off:   self = .on
            case .mixed: self = .on // redefine if necessary and remove this comment
            }
        }
        
        public var color : Color {
            switch self {
            case .off:   return .gray
            default:     return .green
            }
        }
        
        public var isOn : CGFloat {
            switch self {
            case .on:    return 4.5
            case .off:   return -4.5
            case .mixed: return 50
            }
        }
        
        public var cornerRadius : CGFloat {
            switch self {
            case .mixed: return 5
            default:     return 50
            }
        }
        
        public var rectangleSize : CGFloat {
            switch self {
            case .mixed: return 1.33
            default:     return 1.1
            }
        }
        
        public var rotationAngle : Angle {
            switch self {
            case .mixed: return .degrees(45)
            case .on:    return .degrees(90)
            case .off:   return .degrees(0)
            }
        }
    }
}
