import Foundation
import SwiftUI
#if os(macOS)
@available(macOS 12.0, *)
public struct UksColorPicker: View {
    @Binding var color: Color
    
    @State var isPresented = false
    @State var pickerType: PickerType = .circle
    
    let arrowEdge: Edge
    
    public init(color: Binding<Color>, arrowEdge: Edge = .bottom) {
        _color = color
        self.arrowEdge = arrowEdge
    }
    
    @ViewBuilder
    func getIcon(sf: String, type: PickerType) -> some View {
        if pickerType == type {
            Text.sfIcon(sf, size: 20)
                .offset(y: 2)
                .padding(10)
                .shadow(color: .black, radius: 3)
                .background(color)
                .offset(y: -2)
        } else {
            Text.sfIcon(sf, size: 20)
                .offset(y: 2)
                .padding(10)
                .shadow(color: .black, radius: 3)
                .backgroundGaussianBlur(type: .withinWindow , material: .m3_selection)
                .offset(y: -2)
                .onTapGesture {
                    pickerType = type
                }
        }
    }
    
    public var body: some View {
        Circle()
            .fill(color)
            .onTapGesture { isPresented.toggle() }
            .popover(isPresented: $isPresented, arrowEdge: arrowEdge) {
                VStack( spacing: 0) {
                    HStack(spacing: 0) {
                        Space()
                            .fillParent()
                            .padding(.top, 10)
                            .backgroundGaussianBlur(type: .withinWindow, material: .m3_selection)
                            .padding(.top, -10)
                        
                        getIcon(sf: "circle.circle.fill", type: .circle)
                        
                        getIcon(sf: "square.grid.3x3", type: .palette)
                        
                        Space()
                            .fillParent()
                            .padding(.top, 10)
                            .backgroundGaussianBlur(type: .withinWindow, material: .m3_selection)
                            .padding(.top, -10)
                    }
                    .frame(height: 40)
                    .background(color)
                    
                    VStack() {
                        switch pickerType {
                        case .palette:
                            PallettePicker(color: $color, config: PaletteConfig.default)
                            
                            PallettePicker(color: $color, config: PaletteConfig.blackWhite)
                        case .circle:
                            CirclePickerView(color: $color)
                        }
                    }
                    .frame(width: 220, height: 190, alignment: .center)
                    .padding(10)
                    .backgroundGaussianBlur(type: .withinWindow , material: .m1_hudWindow)
                }
            }
    }
}

enum PickerType {
    case palette
    case circle
}
#endif
