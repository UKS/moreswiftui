#if os(macOS)
import Foundation
import SwiftUI

@available(macOS 12.0, *)
struct ColorPipetteBtn: View {
    @ObservedObject var model = CPModel()
    @Binding var color: Color
    @State var hovering = false

    var body: some View {
        Button(action: { model.selectColor(to: $color) } ) {
            Image(decorative: pipetteImg(), scale: 4, orientation: .up)
                .renderingMode(.template)
                .opacity(hovering ? 1 : 0.8)
        }
        .buttonStyle(.plain)
        .onHover{ self.hovering = $0 }
    }
}

@available(macOS 12.0, *)
class CPModel: ObservableObject {
    private let cs = DSFColorSampler()
    
    func selectColor(to color: Binding<Color>) {
        cs.show {
            if let nsc = $0 {
                color.wrappedValue = Color(nsColor: nsc)
            }
        }
    }
}
#endif
