#if os(macOS)
import SwiftUI

typealias MyColor = NSColor

@available(macOS 12.0, *)
public struct ColorPickerRing : View {
    public var color: Binding<Color>
    public var strokeWidth: CGFloat = 30
    
    public var body: some View {
        GeometryReader {
            ColorWheel(color: self.color, frame: $0.frame(in: .local), strokeWidth: self.strokeWidth)
        }
        .aspectRatio(1, contentMode: .fit)
    }
    
    public init(color: Binding<Color>, strokeWidth: CGFloat) {
       self.color = color
       self.strokeWidth = strokeWidth
    }
}

@available(macOS 12.0, *)
public struct ColorWheel: View {
    public var color: Binding<Color>
    public var frame: CGRect
    public var strokeWidth: CGFloat
    
    public var body: some View {
        let indicatorOffset = CGSize(
            width: cos(color.wrappedValue.angle.radians) * Double(frame.midX - strokeWidth / 2),
            height: -sin(color.wrappedValue.angle.radians) * Double(frame.midY - strokeWidth / 2))
        
        return ZStack(alignment: .center) {
            // Color Gradient
            Circle()
                .strokeBorder(AngularGradient.conic, lineWidth: strokeWidth)
                .gesture(DragGesture(minimumDistance: 0, coordinateSpace: .local)
                .onChanged(self.update(value:))
            )
            // Color Selection Indicator
            Circle()
                .fill(color.wrappedValue)
                .frame(width: strokeWidth, height: strokeWidth, alignment: .center)
                .fixedSize()
                .offset(indicatorOffset)
                .allowsHitTesting(false)
                .overlay(
                    Circle()
                        .stroke(Color.white, lineWidth: 3)
                        .offset(indicatorOffset)
                        .allowsHitTesting(false)
            )
        }
    }
    
    public init(color: Binding<Color>, frame: CGRect, strokeWidth: CGFloat) {
       self.frame = frame
       self.color = color
       self.strokeWidth = strokeWidth
    }
    
    func update(value: DragGesture.Value) {
        self.color.wrappedValue = Angle(radians: radCenterPoint(value.location, frame: self.frame)).color.color
    }
    
    func radCenterPoint(_ point: CGPoint, frame: CGRect) -> Double {
        let adjustedAngle = atan2f(Float(frame.midX - point.x), Float(frame.midY - point.y)) + .pi / 2
        return Double(adjustedAngle < 0 ? adjustedAngle + .pi * 2 : adjustedAngle)
    }
}

@available(macOS 12.0, *)
extension Color {
    var angle: Angle {
        Angle(radians: Double(2 * .pi * self.nsColor.hueComponent))
    }
}

#endif
