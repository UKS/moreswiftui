#if os(macOS)
import Foundation
import SwiftUI

@available(macOS 12.0, *)
struct CirclePickerView: View {
    @Binding var color: Color
    
    var body: some View{
        ColorPickerRing(color: $color, strokeWidth: 20)
            .overlay {
                ColorPipetteBtn(color: $color)
            }
    }
}
#endif
