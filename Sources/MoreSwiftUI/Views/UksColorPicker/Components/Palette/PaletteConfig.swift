#if os(macOS)
import Foundation
import SwiftUI

@available(macOS 12.0, *)
struct PaletteConfig {
    let palette: [[RectAndColor]]
    
    let cellSize: NSSize
    
    public let paletteW: Int
    public let paletteH: Int
    
    var border: CGFloat = 4
    
    init(colors: [[Int]], cellSize: NSSize? = nil ) {
        let nsColors = colors.map { $0.compactMap { NSColor(hex: $0) } }
        self.cellSize = cellSize ?? NSSize(width: 18, height: 18)
        
        self.paletteW = Int(self.cellSize.width ) * nsColors[0].count
        self.paletteH = Int(self.cellSize.height) * nsColors.count
        
        self.palette = getPairs(from: nsColors, cellSize: self.cellSize)
    }
    
    func getColorFrom(_ point: CGPoint) -> Color? {
        let rowCell: Int = Int(point.y/cellSize.height)
        let colCell: Int = Int(point.x/cellSize.width)
        
        return palette[safe: rowCell]?[safe: colCell].map{ $0.color.color }
    }
}

@available(macOS 12.0, *)
extension PaletteConfig {
    static var `default`: PaletteConfig {
        let colors: [[Int]] = [
            [0xffffcd, 0xfff8ab, 0xfdee9d,    0xffca00, 0xffb600, 0xffa300,    0xffabdf, 0xff93d4, 0xff7bc7,   0xff9eb0, 0xff88a0, 0xff7190],
            [0xffe76e, 0xffe04a, 0xffd81c,    0xffb600, 0xff7900, 0xff6504,    0xff61be, 0xff44b0, 0xff1ba3,   0xff5b80, 0xff4270, 0xfa235f],
            [0xffcf00, 0xffc800, 0xffbf00,    0xff4f09, 0xff3112, 0xff0114,    0xff0099, 0xff008c, 0xff0080,   0xf5004e, 0xec003e, 0xe6002b],
            
            [0xc5f8ff, 0xa1f4ff, 0x7befff,    0x00ffff, 0x00f0ee, 0x00dddb,    0x00d0ff, 0x00b6f7, 0x009bed,   0xab45d2, 0x9c3cc6, 0x9030bb],
            [0x42eaff, 0x00e5ff, 0x00dfff,    0x00c9c9, 0x00b6b8, 0x00a29f,    0x0083e1, 0x0067d3, 0x004dc8,   0x8129ab, 0x741e9e, 0x661390],
            [0x00dbff, 0x00d5ff, 0x00d1ff,    0x008f8d, 0x007c7a, 0x006967,    0x0033b9, 0x0018ac, 0x0000a0,   0x580785, 0x490177, 0x390069],
            
            [0x77eb96, 0x5ddb89, 0x42cb7d,    0xc1c07a, 0xafae68, 0x9e9d59,    0xea9955, 0xd2884c, 0xba7644,   0xcfe3f0, 0xb8ccd9, 0x9fb5c5],
            [0x24ba6f, 0x00aa63, 0x009a57,    0x8c8a49, 0x7b7937, 0x6a6827,    0xa3653a, 0x8c5433, 0x744226,   0x889eae, 0x708797, 0x597080],
            [0x008b49, 0x00783a, 0x00692e,    0x585614, 0x474500, 0x353200,    0x5a3121, 0x432015, 0x2a0e0f,   0x425969, 0x2b4251, 0x132a3a],
        ]
        
        return PaletteConfig(colors: colors)
    }
    
    static var blackWhite: PaletteConfig {
        let colors: [[Int]] = [
            [0x000000, 0x2a2a2a, 0x3f3f3f,    0x545454, 0x696969, 0x7e7e7e,    0x939393, 0xA8A8A8, 0xBDBDBD,   0xD2D2D2, 0xE7E7E7, 0xffffff].reversed()
        ]
        
        return PaletteConfig(colors: colors)
    }
}

fileprivate func getPairs(from colors: [[NSColor]], cellSize: NSSize) -> [[RectAndColor]] {
    colors.indices.map { rowIdx in
        colors[rowIdx].indices.map { cellIdx -> RectAndColor in
            let x: CGFloat = cellSize.width  * CGFloat(cellIdx) // * 1
            let y: CGFloat = cellSize.height * CGFloat(rowIdx) // * 1
            
            let rect = NSRect(x: x, y: y, width: cellSize.width, height: cellSize.height)
            
            return RectAndColor(rect: rect, color: colors[rowIdx][cellIdx])
        }
    }
}

fileprivate extension Collection {
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
#endif
