#if os(macOS)
import Foundation
import SwiftUI

@available(macOS 12.0, *)
struct PallettePicker : View {
    @Binding var color: Color
    
    let config: PaletteConfig
    
    var body: some View {
        Image(decorative: paletteImg(config), scale: 1, orientation: .downMirrored)
            .gesture(
                DragGesture(minimumDistance: 0, coordinateSpace: .local)
                    .onChanged(self.update(value:))
            )
    }
    
    func update(value: DragGesture.Value) {
        if let color = config.getColorFrom(value.location) {
            if self.color != color {
                self.color = color
            }
        }
    }
}
#endif
