import Foundation
import SwiftUI

public struct ExecuteCode : View {
    public init( _ codeToExec: () -> () ) {
        codeToExec()
    }
    
    public var body: some View {
        return EmptyView()
    }
}
