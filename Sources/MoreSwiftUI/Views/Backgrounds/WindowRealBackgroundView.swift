import Foundation
import SwiftUI
#if os(macOS)
///This view displays blended color of default window background
@available(macOS 11.0, *)
public struct WindowRealBackgroundView: NSViewRepresentable {
    @State var material: NSVisualEffectView.Material = .underWindowBackground
    @State var blendingMode: NSVisualEffectView.BlendingMode = .withinWindow
    
    public init() {}
    
    public func makeNSView(context: Context) -> NSVisualEffectView {
        let view = NSVisualEffectView()
        view.material = material
        view.blendingMode = blendingMode
        return view
    }
    
    public func updateNSView(_ nsView: NSVisualEffectView, context: Context) {
        nsView.material = material
        nsView.blendingMode = blendingMode
    }
}
#endif
