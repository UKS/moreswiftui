import Foundation
import SwiftUI

public struct AuroraTheme {
    var bg: Color
    
    var ellipsesTopLeading: Color
    var ellipsesTopTrailing: Color
    var ellipsesBottomTrailing: Color
    var ellipsesBottomLeading: Color
    
    public init(bg: Color, ellipsesTopLeading: Color, ellipsesTopTrailing: Color, ellipsesBottomTrailing: Color, ellipsesBottomLeading: Color) {
        self.bg = bg
        self.ellipsesTopLeading = ellipsesTopLeading
        self.ellipsesTopTrailing = ellipsesTopTrailing
        self.ellipsesBottomTrailing = ellipsesBottomTrailing
        self.ellipsesBottomLeading = ellipsesBottomLeading
    }
}

public extension AuroraTheme {
    static var standard: AuroraTheme {
        AuroraTheme(bg: Color(red: 0.043, green: 0.467, blue: 0.494),
                    ellipsesTopLeading: Color(red: 0.000, green: 0.176, blue: 0.216, opacity: 80.0),
                    ellipsesTopTrailing: Color(red: 0.408, green: 0.698, blue: 0.420, opacity: 0.61),
                    ellipsesBottomTrailing: Color(red: 0.541, green: 0.733, blue: 0.812, opacity: 0.7),
                    ellipsesBottomLeading: Color(red: 0.525, green: 0.859, blue: 0.655, opacity: 0.45))
    }
}





public struct AuroraClouds: View {
    @Environment(\.colorScheme) var scheme
    let blur: CGFloat
    let theme: AuroraTheme
    @Binding var animationsEnabled: Bool
    
    public init(blur: CGFloat = 60, theme: AuroraTheme = AuroraTheme.standard, animationsEnabled: Binding<Bool> ) {
        self.blur = blur
        self.theme = theme
        _animationsEnabled = animationsEnabled
    }
    
    public var body: some View {
        GeometryReader { proxy in
            ZStack {
                theme.bg
                
                ZStack {
                    Cloud(proxy: proxy,
                          color: theme.ellipsesBottomTrailing,
                          rotationStart: 0,
                          duration: 60,
                          alignment: .bottomTrailing, 
                          animationsEnabled: animationsEnabled)
                    Cloud(proxy: proxy,
                          color: theme.ellipsesTopTrailing,
                          rotationStart: 240,
                          duration: 50,
                          alignment: .topTrailing,
                          animationsEnabled: animationsEnabled)
                    Cloud(proxy: proxy,
                          color: theme.ellipsesBottomLeading,
                          rotationStart: 120,
                          duration: 80,
                          alignment: .bottomLeading,
                          animationsEnabled: animationsEnabled)
                    Cloud(proxy: proxy,
                          color: theme.ellipsesTopLeading,
                          rotationStart: 180,
                          duration: 70,
                          alignment: .topLeading,
                          animationsEnabled: animationsEnabled)
                }
                .blur(radius: blur)
            }
            .ignoresSafeArea()
        }
    }
}

private class CloudProvider: ObservableObject {
    let offset: CGSize
    let frameHeightRatio: CGFloat
    
    init() {
        frameHeightRatio = CGFloat.random(in: 0.7 ..< 1.4)
        offset = CGSize(width: CGFloat.random(in: -150 ..< 150),
                        height: CGFloat.random(in: -150 ..< 150))
    }
}

private struct Cloud: View {
    @StateObject var provider = CloudProvider()
    @State var move = false
    let proxy: GeometryProxy
    let color: Color
    let rotationStart: Double
    let duration: Double
    let alignment: Alignment
    let animationsEnabled: Bool
    
    var body: some View {
        Circle()
            .fill(color)
            .frame(height: proxy.size.height /  provider.frameHeightRatio)
            .offset(provider.offset)
            .rotationEffect(.init(degrees: move ? rotationStart : rotationStart + 360) )
            .animation(Animation.linear(duration: duration).repeatForever(autoreverses: false), value: move)
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: alignment)
            .opacity(0.8)
            .onAppear {
                if animationsEnabled {
                    move.toggle()
                }
            }
    }
}

//private struct AuroraTheme {
//    static var generalBackground: Color {
//        Color(red: 0.043, green: 0.467, blue: 0.494)
//    }
//    
//    static func ellipsesTopLeading(forScheme scheme: ColorScheme) -> Color {
//        let any = Color(red: 0.039, green: 0.388, blue: 0.502, opacity: 0.81)
//        let dark = Color(red: 0.000, green: 0.176, blue: 0.216, opacity: 80.0)
//        switch scheme {
//        case .light:
//            return any
//        case .dark:
//            return dark
//        @unknown default:
//            return any
//        }
//    }
//    
//    static func ellipsesTopTrailing(forScheme scheme: ColorScheme) -> Color {
//        let any = Color(red: 0.196, green: 0.796, blue: 0.329, opacity: 0.5)
//        let dark = Color(red: 0.408, green: 0.698, blue: 0.420, opacity: 0.61)
//        switch scheme {
//        case .light:
//            return any
//        case .dark:
//            return dark
//        @unknown default:
//            return any
//        }
//    }
//    
//    static func ellipsesBottomTrailing(forScheme scheme: ColorScheme) -> Color {
//        Color(red: 0.541, green: 0.733, blue: 0.812, opacity: 0.7)
//    }
//    
//    static func ellipsesBottomLeading(forScheme scheme: ColorScheme) -> Color {
//        let any = Color(red: 0.196, green: 0.749, blue: 0.486, opacity: 0.55)
//        let dark = Color(red: 0.525, green: 0.859, blue: 0.655, opacity: 0.45)
//        switch scheme {
//        case .light:
//            return any
//        case .dark:
//            return dark
//        @unknown default:
//            return any
//        }
//    }
//}
