import SwiftUI
#if canImport(UIKit)
import UIKit
/// SwifterSwift: Font
public typealias SFFont = UIFont
#endif

#if canImport(AppKit) && !targetEnvironment(macCatalyst)
import AppKit
/// SwifterSwift: Font
public typealias SFFont = NSFont
#endif

#if os(OSX)
@available(macOS 12.0, *)
public struct DropDown<T>: View where T: Comparable  {
    @Binding public var selection: T
    private var selectionInternal: Binding<Int> {
        Binding<Int>(
            get: { data.firstIndexInt(where: { $0 == selection }) ?? -1 },
            set: { newVal in
                self.selection = data[newVal]
            }
        )
    }
    
    public var data: [T]
    public var style: DropDownStyle = .default
    
    public var body: some View {
        PickerUiKitView(selection: selectionInternal, data: data, style: style)
            .if(style.colorMultiply != nil) {
                $0.colorMultiply(style.colorMultiply ?? .white)
            }
    }
}

@available(macOS 12.0, *)
struct PickerUiKitView<T>: NSViewRepresentable where T: Comparable {
    @Binding var selection: Int
    var data: [T]
    let style: DropDownStyle
    
    class Coordinator: NSObject {
        var parent: PickerUiKitView
        
        init(parent: PickerUiKitView) {
            self.parent = parent
        }
        
        @objc func valueChanged(_ sender: NSPopUpButton) {
            parent.selection = sender.indexOfSelectedItem
        }
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(parent: self)
    }
    
    func makeNSView(context: Context) -> NSPopUpButton {
        let picker = NSPopUpButton()
        
        let data = self.data.map{ "\($0)" }
        
        picker.addItems(withTitles: data)
        picker.selectItem(at: selection)
        picker.target = context.coordinator
        
        picker.action = #selector(Coordinator.valueChanged(_:))
        
        return picker
    }
    
    func updateNSView(_ nsView: NSPopUpButton, context: Context) {
        nsView.removeAllItems()
        let data = self.data.map{ "\($0)" }
        nsView.addItems(withTitles: data)
        nsView.selectItem(at: selection)
        
        customizePicker(nsView)
    }
    
    private func customizePicker(_ picker: NSPopUpButton) {
        picker.bezelStyle = .accessoryBarAction
        
        var attributes: [NSAttributedString.Key: Any] = [:]
        
        if let font = style.font {
            attributes.updateValue(font, forKey: .font)
            picker.font = font
        }
        
        if let foregrColor = style.fontColor?.nsColor {
            attributes.updateValue(foregrColor, forKey: .foregroundColor)
        }
        
        for (index, _) in data.enumerated() {
            let attributedTitle = NSAttributedString(string: "\(data[index])", attributes: attributes)
            picker.item(at: index)?.attributedTitle = attributedTitle
        }
    }
}
#endif

public struct DropDownStyle {
    let font: SFFont?
    let fontColor: Color?
    let colorMultiply: Color?
}

public extension DropDownStyle {
    static var `default` = DropDownStyle(font: nil, fontColor: nil, colorMultiply: nil)
}
