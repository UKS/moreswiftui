import SwiftUI
import Essentials

public struct LoadingView : View {
    public init(){ }
    
    let baseAnimation = Animation.linear(duration: 1)
    @State var isTransparent = true
    
    public var body: some View {
        ProgressView()
            .animationMovementDisable()
            .progressViewStyle(CircularProgressViewStyle())
            .opacity(isTransparent ? 0 : 1)
            .animation(baseAnimation, value: isTransparent)
            .onAppear() {
                isTransparent = false
            }
    }
}

public struct LoadingView2: View {
    @State var value: CGFloat = 0
    
    let timer = TimerPublisher(every: 0.3)
    
    public init() { }
    
    public var body: some View {
        ProgressView(value: value, total: 100)
            .progressViewStyle(.circular)
            .onReceive(timer) { _ in
                withAnimation {
                    if value >= 100 {
                        value = 5
                    } else {
                        let newValue = value + CGFloat.random(in: 1..<25)
                        value = newValue < 100 ? newValue : 100
                    }
                }
            }
            .animation( Animation.easeInOut(duration: 0.3), value: value )
    }
}

public struct LockedUIView: View {
    public init(){ }
    
    let baseAnimation = Animation.linear(duration: 1)
    @State var isTransparent = true
    
    public var body: some View {
        RoundedRectangle(cornerRadius: 4)
            .fill(Color.black)
            .blur(radius: 7.0)
            .opacity(isTransparent ? 0 : 0.3)
            .overlay( LoadingView() )
            // Disable clicks through the locking view
            .onTapGesture { }
            .animation(baseAnimation, value: isTransparent)
            .onAppear() {
                isTransparent = false
            }
    }
}

public extension View {
    @ViewBuilder
    func loadingMod( inProgress: Bool, scale: CGFloat = 1 ) -> some View {
        ZStack {
            self
                .disabled(inProgress)
                .animation(Animation.linear(duration: 1), value: inProgress)
                .blur(radius: inProgress ? 5 : 0)
            
            if inProgress {
                LoadingView()
                    .scaleEffect(scale)
            }
        }
    }
    
    func loadingModLockUi() -> some View {
        self.overlay( LockedUIView() )
    }
}
