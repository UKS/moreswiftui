import SwiftUI

@available(macOS 12.0, *)
public struct PopoverButt<Label: View, Content: View>: View {
    let edge : Edge
    @Environment(\.colorScheme) var scheme
    @ViewBuilder var content: () -> Content
    @ViewBuilder var label: () -> Label
    
    @Binding var isPresented: Bool
    
    public init(edge: Edge, isPresented: Binding<Bool>,  @ViewBuilder _ label: @escaping () -> Label,
                @ViewBuilder content: @escaping () -> Content) {
        self.edge = edge
        self.label = label
        self.content = content
        self._isPresented = isPresented
    }
    
    public init(edge: Edge, isPresented: Binding<Bool>, @ViewBuilder content: @escaping () -> Content) where Label == EmptyView {
        self.edge = edge
        self.label = { EmptyView() }
        self.content = content
        self._isPresented = isPresented
    }
    
    public var body: some View {
        Button(action:{ self.isPresented.toggle() }, label: label)
            .popover(isPresented: $isPresented, arrowEdge: edge) {
                content()
                    .background (
                        ZStack {
                            if scheme == .light {
                                Rectangle()
                                    .foregroundColor(.gray)
                                    .opacity(0.25)
                                
                                Rectangle()
                                    .foregroundColor(.white)
                                    .opacity(0.5)
                            } else {
                                Rectangle()
                                    .foregroundColor(.gray)
                                    .opacity(0.25)
                                
                                Rectangle().foregroundColor(.black)
                                    .opacity(0.3)
                            }
                        }
                    )
            }
            //.cursorPointHand()
    }
}
