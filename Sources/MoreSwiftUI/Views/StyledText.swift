import SwiftUI

/*
 USAGE:
 
 StyledText(verbatim: String(localized: "Some text Accessability some text ") )
     .style(.foregroundColor(colors.preludeText))
     .style(.italic())
     .style(.foregroundColor(Color.systemOrange), range: { $0.range(of: "Accessability")! } )
 */

@available(OSX 11.0, *)



public struct StyledText {
    // This is a value type. Don't be tempted to use NSMutableAttributedString here unless
    // you also implement copy-on-write.
    private var attributedString: NSAttributedString

    
    ///```
    ///StyledText(verbatim: "👩‍👩‍👦someText1")
    ///    .style(.highlight(), ranges: { [$0.range(of: "eTex")!, $0.range(of: "1")!] })
    ///    .style(.bold())
    ///```
    private init(attributedString: NSAttributedString) {
        self.attributedString = attributedString
    }

    //{ [$0.range(of: status.fileDir), $0.range(of: status.fileDir)] }
    func style<S>(_ style: TextStyle, rangeFromTextBlock: (String) -> (S)) -> StyledText where S: Sequence, S.Element == Range<String.Index>?
    {
        // Remember this is a value type. If you want to avoid this copy,
        // then you need to implement copy-on-write.
        let newAttributedString = NSMutableAttributedString(attributedString: attributedString)
        
        for rangeIdx in rangeFromTextBlock(attributedString.string) {
            if let rangeIdx = rangeIdx {
                let nsRange = NSRange(rangeIdx, in: attributedString.string)
                newAttributedString.addAttribute(style.key, value: style, range: nsRange)
            }
        }

        return StyledText(attributedString: newAttributedString)
    }
    
    public func style(_ mystyle: TextStyle, phrases: [String], ignoreCase:Bool = false ) -> StyledText {
        if ignoreCase {
            return style(mystyle) { text in
                phrases.map { text.lowercased().range(of: $0.lowercased()) }
            }
        }
        
        return style(mystyle) { text in
            phrases.map { text.range(of: $0) }
        }
    }
    
}

@available(OSX 11.0, *)
public extension StyledText {
    // A convenience extension to apply to a single range.
    func style(_ style: TextStyle, range: (String) -> Range<String.Index>? = { $0.startIndex..<$0.endIndex }) -> StyledText {
        self.style(style, rangeFromTextBlock: { [range($0)].compactMap{ $0 } })
    }
}

@available(OSX 11.0, *)
public extension StyledText {
    ///```
    ///StyledText(verbatim: "👩‍👩‍👦someText1")
    ///    .style(.highlight(), ranges: { [$0.range(of: "eTex")!, $0.range(of: "1")!] })
    ///    .style(.bold())
    ///```
    init(verbatim content: String, styles: [TextStyle] = []) {
        let attributes = styles.reduce(into: [:]) { result, style in
            result[style.key] = style
        }
        attributedString = NSMutableAttributedString(string: content, attributes: attributes)
    }
}

@available(OSX 11.0, *)
extension StyledText: View {
    public var body: some View { text() }

    public func text() -> Text {
        var text: Text = Text(verbatim: "")
        attributedString
            .enumerateAttributes(in: NSRange(location: 0, length: attributedString.length),
                                 options: [])
            { (attributes, range, _) in
                let string = attributedString.attributedSubstring(from: range).string
                let modifiers = attributes.values.map { $0 as! TextStyle }
                text = text + modifiers.reduce(Text(verbatim: string)) { segment, style in
                    style.apply(segment)
                }
        }
        return text
    }
}




// ---------------- TEXT STYLE ------------------//
@available(OSX 11.0, *)
public struct TextStyle {
    // This type is opaque because it exposes NSAttributedString details and requires unique keys.
    // It can be extended, however, by using public static methods.
    // Properties are internal to be accessed by StyledText
    internal let key: NSAttributedString.Key
    internal let apply: (Text) -> Text
    private init(key: NSAttributedString.Key, apply: @escaping (Text) -> Text) {
        self.key = key
        self.apply = apply
    }
}

// Public methods for building styles
@available(OSX 11.0, *)
public extension TextStyle {
    static func foregroundColor(_ color: Color) -> TextStyle {
        TextStyle(key: .init("TextStyleForegroundColor"), apply: { $0.foregroundColor(color) })
    }
    
    static func bold() -> TextStyle {
        TextStyle(key: .init("TextStyleBold"), apply: { $0.bold() })
    }
    
    static func italiccc() -> TextStyle {
        TextStyle(key: .init("TextStyleItalic"), apply: { $0.italic() })
    }
    
    static func font(_ font: String, size: CGFloat)-> TextStyle {
        TextStyle(key: .font, apply: { $0.font(Font(font, size: size) ) })
    }
    
    static func font(_ font: Font) -> TextStyle {
        TextStyle(key: .font, apply: { $0.font(font ) })
    }
    
    static func charSpacing(_ kern: CGFloat) -> TextStyle {
        TextStyle(key: .init("TextKerning") , apply: { $0.kerning(kern) })
    }
}

// An internal convenience extension that could be defined outside this pacakge.
// This wouldn't be a general-purpose way to highlight, but shows how a caller could create
// their own extensions
@available(OSX 11.0, *)
extension TextStyle {
    public static func highlight(_ color: Color = .red) -> TextStyle { .foregroundColor(color) }
    public static func italic() -> TextStyle { .italiccc() }
}

public extension Font {
    init(_ font: String, size: CGFloat) {
        self.init(CTFontCreateWithName(font as CFString, size, nil))
    }
}
