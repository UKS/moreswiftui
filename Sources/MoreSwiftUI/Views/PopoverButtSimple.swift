import Foundation
import SwiftUI

public struct PopoverButtSimple<Label: View, Content: View>: View {
    @State private  var isPresented: Bool = false
    
    @ViewBuilder var label: () -> Label
    @ViewBuilder var content: () -> Content
    
    @Environment(\.isEnabled) var isEnabled
    
    public init(label: @escaping () -> Label, content: @escaping () -> Content) {
        self.label = label
        self.content = content
    }
    
    public var body: some View {
        Button(action:{ if isEnabled { self.isPresented.toggle() } }, label: label)
            .popover(isPresented: $isPresented, content: content )
            .opacity(isEnabled ? 1 : 0.6)
     }
 }
