
import SwiftUI

public struct UksWheelPickerSelectBg: View {
    let custColor: Color?
    let height: CGFloat
    
    var color: Color {
        if let custColor {
            return custColor
        }
        
        #if os(iOS)
            return Color(.systemGray5).opacity(0.8)
        #elseif os(macOS)
            return Color(NSColor.unemphasizedSelectedContentBackgroundColor).opacity(0.8)
        #endif
    }
    
    public init(height: CGFloat, color: Color? = nil) {
        self.height = height
        self.custColor = color
    }
    
    public var body: some View {
        #if os(iOS)
            SelectionLine()
        #endif
        
        #if os(macOS)
        if #available(macOS 14.0, *) {
            SelectionLine()
        } else {
            EmptyView()
        }
        #endif
    }
    
    func SelectionLine() -> some View {
        color
            .cornerRadius(4)
            .frame(height: height)
    }
}
