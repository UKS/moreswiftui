
import SwiftUI

public struct UksWheelConfig {
    public let itemHeight: CGFloat
    public let selectionEnabled: Bool
    public let selectionColor: Color?
    public let selectionIsOverlay: Bool
    
    public init(itemHeight: CGFloat, selectionEnabled: Bool = true, selectionColor: Color? = nil, selectionIsOverlay: Bool = false) {
        self.itemHeight = itemHeight
        self.selectionEnabled = selectionEnabled
        self.selectionColor = selectionColor
        self.selectionIsOverlay = selectionIsOverlay
    }
}
