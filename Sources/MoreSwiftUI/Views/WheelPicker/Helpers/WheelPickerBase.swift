
import SwiftUI
import Essentials

@available(macOS 14.0, *)
@available(iOS 17.0, *)
struct WheelPickerBase<T, ItemView>: View where T: CustomStringConvertible, T: Hashable, ItemView: View {
    @Binding var selected: T?
    var items: [T]
    
    let config: UksWheelConfig
    
    var itemView: (T) -> ItemView
    
    ///usage:
    /// ```
    /// UksWheelPicker(selected: selection, items: items, config: config) { item in
    ///    Text("\(item)")
    ///        .font(.custom("SF Pro", size: 18))
    ///        .frame(minWidth: 100)
    /// }
    ///```
    init(selected: Binding<T?>, items: [T], config: UksWheelConfig, itemView: @escaping (T) -> ItemView) {
        _selected = selected
        self.items = items
        self.config = config
        self.itemView = itemView
    }
    
    var body: some View {
        ScrollViewReader { reader in
            ScrollView(.vertical) {
                LazyVStack(spacing: 0) {
                    ForEach(items, id: \.self) { item in
                        itemView(item)
                            .frame(height: config.itemHeight)
                            .id(item)
                            .onTapGesture {
                                withAnimation {
                                    selected = item
                                }
                            }
                    }
                }
                .padding(.vertical, config.itemHeight * 3) // 👈 added
                .scrollTargetLayout()
            }
            .scrollTargetBehavior( // 👈 updated
                StickyMiddlePosition(itemHeight: config.itemHeight, spacing: 0, verticalPadding: config.itemHeight * 3)
            )
            .scrollIndicators(.hidden)
            .scrollPosition(id: $selected, anchor: .center)
            .frame(height: config.itemHeight * 7)
            .mask(LinearGradient(gradient: Gradient(colors: [.clear, .clear, .black, .black, .clear, .clear]), startPoint: .top, endPoint: .bottom))
            .modify { view in
                if config.selectionEnabled {
                    if config.selectionIsOverlay {
                        view.overlay(alignment: .center, content: {
                            UksWheelPickerSelectBg(height: config.itemHeight + 3, color: config.selectionColor)
                        })
                    } else {
                        view.background {
                            UksWheelPickerSelectBg(height: config.itemHeight + 3, color: config.selectionColor)
                        }
                    }
                } else {
                    view
                }
            }
            .onAppear {
                scrollToNeededElem(reader: reader)
            }
        }
        .onChange(of: selected) { _ in
            #if os(iOS)
                UksHaptic.vibrate(style: .light)
            #elseif os(macOS)
                UksHaptic.vibrate(style: .generic)
            #endif
        }
    }
}

@available(macOS 14.0, *)
@available(iOS 17.0, *)
fileprivate struct StickyMiddlePosition: ScrollTargetBehavior {
    let itemHeight: CGFloat
    let spacing: CGFloat
    let verticalPadding: CGFloat
    
    func updateTarget(_ target: inout ScrollTarget, context: TargetContext) {
        // dy is the distance from the target anchor to the
        // top edge of a centered item
        let dy = (target.anchor?.y ?? 0) == 0
            ? (context.containerSize.height / 2) - (itemHeight / 2)
            : 0
        let currentTargetIndex = (target.rect.origin.y + dy - verticalPadding) / (itemHeight + spacing)
        let roundedTargetIndex = currentTargetIndex.rounded()
        let scrollCorrection = (roundedTargetIndex - currentTargetIndex) * (itemHeight + spacing)
        target.rect.origin.y += scrollCorrection
    }
}

//////////////////////
///HELPERS
/////////////////////

@available(macOS 14.0, *)
@available(iOS 17.0, *)
extension WheelPickerBase {
    func scrollToNeededElem(reader: ScrollViewProxy) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            withAnimation {
                if let selected {
                    reader.scrollTo(selected, anchor: .center)
                }
            }
        }
    }
}
