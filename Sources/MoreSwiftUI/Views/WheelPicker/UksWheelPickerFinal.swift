
import SwiftUI

@available(macOS 14.0, *)
public struct UksWheelPickerFinal<Item: Hashable, ItemView: View>: View where Item: CustomStringConvertible {
    @Binding var selection: Item?
    var items: [Item]
    let config: UksWheelConfig
    
    let itemView: (Item) -> ItemView
    
    public init(selection: Binding<Item?>, items: [Item], config: UksWheelConfig, view: @escaping (Item) -> ItemView) {
        _selection = selection
        self.items = items
        self.config = config
        self.itemView = view
    }
    
    public var body: some View {
        PickerWrapped()
    }
    
    #if os(macOS)
    @ViewBuilder
    private func PickerWrapped() -> some View {
        WheelPickerBase(selected: $selection, items: items, config: config) { item in
            itemView(item)
        }
    }
    #endif
    
    #if os(iOS)
    @ViewBuilder
    private func PickerWrapped() -> some View {
        if #available(iOS 17.0, *) {
            UksWheelPicker(selected: $selection, items: items, config: config) { item in
                itemView(item)
            }
        } else {
            Picker("", selection: $selection) {
                ForEach(items.indices, id: \.self) {
                    itemView(items[$0])
                        .tag(items[$0])
                }
            }
            .pickerStyle(.wheel)
            .colorMultiply(RLColors.brownLight)
        }
    }
    #endif
}
