import Foundation
import SwiftUI
import AVKit
import AVFoundation
#if os(macOS)
@available(macOS 11.0, *)
public struct GifyVideoView : View {
    let videoName: String
    let ext: String
    
    public init(videoName: String, extension ext: String) {
        self.videoName = videoName
        self.ext = ext
    }
    
    public var body: some View {
        if let url = Bundle.main.url(forResource: videoName, withExtension: ext) {
            GifyVideoViewWrapped(url: url)
                .makeFullyIntaractable()
        } else {
            Text("Can't open video resource \"\(videoName)\"")
        }
    }
}

@available(macOS 11.0, *)
fileprivate struct GifyVideoViewWrapped: View {
    let player : AVPlayer
    
    var pub = NotificationCenter.default.publisher(for: .AVPlayerItemDidPlayToEndTime)
    
    init(url: URL) {
        player = AVPlayer(url: url)
    }
    
    var body: some View {
        AVPlayerControllerRepresented(player: player)
            .onAppear {
                player.play()
                player.volume = 0
            }
            .onReceive(pub) { (output) in
                player.play()
            }
    }
}

fileprivate struct AVPlayerControllerRepresented : NSViewRepresentable {
    var player : AVPlayer
    
    func makeNSView(context: Context) -> AVPlayerView {
        let view = AVPlayerView()
        view.controlsStyle = .none
        view.player = player
        
        return view
    }
    
    func updateNSView(_ nsView: AVPlayerView, context: Context) { }
}
#endif
