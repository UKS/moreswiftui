import SwiftUI
import Combine

@available(iOS 14.0, *)
@available(macOS 12.0, *)
public struct DotsTabView<T: CaseIterable & Equatable>: View where T: Hashable {
    @State var animationRight: Bool = true
    private let tab: Binding<T>
    private let views: [AnyView]
    private let customFinish: (() -> ())?
    
    @Namespace private var namespace
    
    public init<A: View, B: View> (tab: Binding<T>, customFinish: (() -> ())? = nil, @ViewBuilder content: () -> TupleView<(A, B)>) {
        self.tab = tab
        self.customFinish = customFinish
        let views = content().value
        self.views = [AnyView(views.0), AnyView(views.1)]
    }
    
    public init<A: View, B: View, C: View>(tab: Binding<T>, customFinish: (() -> ())? = nil, @ViewBuilder content: () -> TupleView<(A, B, C)>) {
        self.tab = tab
        self.customFinish = customFinish
        let views = content().value
        self.views = [AnyView(views.0), AnyView(views.1), AnyView(views.2)]
    }
    
    public init<A: View, B: View, C: View, D: View>(tab: Binding<T>, customFinish: (() -> ())? = nil, @ViewBuilder content: () -> TupleView<(A, B, C, D)>) {
        self.tab = tab
        self.customFinish = customFinish
        let views = content().value
        self.views = [AnyView(views.0), AnyView(views.1), AnyView(views.2), AnyView(views.3)]
    }
    
    public init<A: View, B: View, C: View, D: View, E: View>(tab: Binding<T>, customFinish: (() -> ())? = nil, @ViewBuilder content: () -> TupleView<(A, B, C, D, E)>) {
        self.tab = tab
        self.customFinish = customFinish
        let views = content().value
        self.views = [AnyView(views.0), AnyView(views.1), AnyView(views.2), AnyView(views.3), AnyView(views.4)]
    }
    
    public init<A: View, B: View, C: View, D: View, E: View, F: View>(tab: Binding<T>, customFinish: (() -> ())? = nil, @ViewBuilder content: () -> TupleView<(A, B, C, D, E, F)>) {
        self.tab = tab
        self.customFinish = customFinish
        let views = content().value
        self.views = [AnyView(views.0), AnyView(views.1), AnyView(views.2), AnyView(views.3), AnyView(views.4), AnyView(views.5)]
    }
    
    public init<A: View, B: View, C: View, D: View, E: View, F: View, G: View>(tab: Binding<T>, customFinish: (() -> ())? = nil, @ViewBuilder content: () -> TupleView<(A, B, C, D, E, F, G)>) {
        self.tab = tab
        self.customFinish = customFinish
        let views = content().value
        self.views = [AnyView(views.0), AnyView(views.1), AnyView(views.2), AnyView(views.3), AnyView(views.4), AnyView(views.5), AnyView(views.6)]
    }
    
    public init<A: View, B: View, C: View, D: View, E: View, F: View, G: View, H: View>(tab: Binding<T>, customFinish: (() -> ())? = nil, @ViewBuilder content: () -> TupleView<(A, B, C, D, E, F, G, H)>) {
        self.tab = tab
        self.customFinish = customFinish
        let views = content().value
        self.views = [AnyView(views.0), AnyView(views.1), AnyView(views.2), AnyView(views.3), AnyView(views.4), AnyView(views.5), AnyView(views.6), AnyView(views.7)]
    }
    
    public init<A: View, B: View, C: View, D: View, E: View, F: View, G: View, H: View, I: View>(tab: Binding<T>,customFinish: (() -> ())? = nil, @ViewBuilder content: () -> TupleView<(A, B, C, D, E, F, G, H, I)>) {
        self.tab = tab
        self.customFinish = customFinish
        let views = content().value
        self.views = [AnyView(views.0), AnyView(views.1), AnyView(views.2), AnyView(views.3), AnyView(views.4), AnyView(views.5), AnyView(views.6), AnyView(views.7), AnyView(views.8)]
    }
    
    public init<A: View, B: View, C: View, D: View, E: View, F: View, G: View, H: View, I: View, J: View>(tab: Binding<T>, customFinish: (() -> ())? = nil, @ViewBuilder content: () -> TupleView<(A, B, C, D, E, F, G, H, I, J)>) {
        self.tab = tab
        self.customFinish = customFinish
        let views = content().value
        self.views = [AnyView(views.0), AnyView(views.1), AnyView(views.2), AnyView(views.3), AnyView(views.4), AnyView(views.5), AnyView(views.6), AnyView(views.7), AnyView(views.8), AnyView(views.9)]
    }
    
    public init<Data, Content: View, ID : Hashable>(tab: Binding<T>, customFinish: (() -> ())? = nil, @ViewBuilder content: () -> ForEach<Data, ID, Content>) {
        self.tab = tab
        self.customFinish = customFinish
        let views = content()
        self.views = views.data.map({ AnyView(views.content($0)) })
    }
    
    public var body: some View {
        let countMin = min(T.allCases.count, views.count)
        let selectedIdx = T.firstIdxOf(t: tab.wrappedValue)
        
        #if os(macOS)
        macOSView(countMin: countMin, selectedIdx: selectedIdx)
        #elseif os(iOS)
        MobView(countMin: countMin, selectedIdx: selectedIdx)
        #endif
    }
    
    func macOSView(countMin: Int, selectedIdx: Int) -> some View {
        VStack {
            views[ min(selectedIdx, countMin) ]
                .modify {
                    if #available(macOS 13.0, *) {
                        $0.transition( .push(from: animationRight ? .trailing : .leading) )
                    } else {
                        $0
                    }
                }
                .onSwipe { event in
                    switch event.direction {
                    case .left:
                        prevAction()
                    case .right:
                        nextAction(count: countMin)
                    default:
                        break
                    }
                }
            
            TabPoints(countMin, selectedIdx: selectedIdx )
                .padding(.bottom,10)
        }
    }
    #if os(iOS)
    func MobView(countMin: Int, selectedIdx: Int) -> some View {
        VStack {
            TabView(selection: tab) {
                ForEach(Array(T.allCases), id: \.self) { item in
                    if let index = T.allCases.firstIndex(of: item) {
                        views[index as! Int]
                            .tag(item)
                    }
                }
            }
            .tabViewStyle(.page(indexDisplayMode: .never))
// DOES NOT WORK
//            .onSwipe { swipe in
//                guard tab.wrappedValue == T.allCases[T.allCases.endIndex] else { return }
//                
//                switch swipe.direction {
//                case .left:
//                    self.customFinish?()
//                default:
//                    break
//                }
//            }
            
            TabPoints(countMin, selectedIdx: selectedIdx )
        }
    }
    #endif
    @ViewBuilder
    private func TabPoints(_ count: Int, selectedIdx: Int) -> some View {
        HStack(spacing: 15) {
            PrevBtn(selectedIdx: selectedIdx)
            
            ForEach(0..<count, id: \.self) { idx in
                Circle()
                    .stroke(.white.opacity(0.5))
                    .makeFullyIntaractable()
                    .frame(width: 10, height: 10)
                    .onTapGesture {
                        self.tab.wrappedValue = T.allCases[idx as! T.AllCases.Index]
                    }
                    .matchedGeometryEffect(id: idx, in: namespace, isSource: selectedIdx == idx)
            }
            
            NextBtn(count: count, selectedIdx: selectedIdx)
        }
        .background {
            Circle()
                .fill(.white.opacity(0.9))
                .shadow(color: .yellow, radius: 3)
                .matchedGeometryEffect(id: selectedIdx, in: namespace, isSource: false)
                .animation(.easeInOut(duration: 0.1), value: selectedIdx)
                .opacity(0.8)
        }
    }
}

fileprivate extension CaseIterable {
    static func firstIdxOf(t: Self) -> Int where Self: Equatable {
        for i in Self.allCases.indices {
            if Self.allCases[i] == t {
                return i as! Int
            }
        }
        
        return 0
    }
}

@available(macOS 12.0, *)
@available(iOS 14.0, *)
fileprivate extension DotsTabView {
    func PrevBtn(selectedIdx: Int) -> some View {
        Button(action: { prevAction() }) {
            Text.sfIcon("triangle.fill", size: 20)
                .rotationEffect(.degrees(30))
        }
        .buttonStyle(.plain)
        .opacity(selectedIdx > 0 ? 1 : 0.5)
        .offset(x: 0, y: -1)
        .animation(.easeInOut, value: self.tab.wrappedValue)
        .padding(.horizontal)
    }
    
    func NextBtn(count: Int, selectedIdx: Int) -> some View {
        Button(action: { nextAction(count: count) }) {
            Text.sfIcon("triangle.fill", size: 20)
                .rotationEffect(.degrees(-30))
        }
        .buttonStyle(.plain)
        .opacity( (selectedIdx < count - 1 || customFinish != nil) ? 1 : 0.5)
        .offset(x: 0, y: -1)
        .animation(.easeInOut, value: self.tab.wrappedValue)
        .padding(.horizontal)
    }
}

@available(macOS 12.0, *)
@available(iOS 14.0, *)
fileprivate extension DotsTabView {
    func prevAction() {
        animationRight = false
        
        let selectedIdx = T.firstIdxOf(t: tab.wrappedValue)
        
        if selectedIdx > 0 {
            withAnimation {
                self.tab.wrappedValue = T.allCases[(selectedIdx - 1) as! T.AllCases.Index]
            }
        }
     }
     
    func nextAction(count: Int) {
        animationRight = true
        
        let selectedIdx = T.firstIdxOf(t: tab.wrappedValue)
        
        if selectedIdx < count - 1 {
            withAnimation{
                self.tab.wrappedValue = T.allCases[(selectedIdx + 1) as! T.AllCases.Index]
            }
        } else if let customFinish = customFinish {
            customFinish()
        }
    }
}
