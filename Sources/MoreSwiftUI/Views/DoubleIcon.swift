import Foundation
import SwiftUI

@available(macOS 12.0, *)
public struct DoubleIcon: View {
    let icon: String
    let iconColor: Color?
    let iconSize: CGFloat
    
    let subIcon: String
    let subIconColor: Color?
    let subIconSize: CGFloat
    let offsetX: CGFloat
    let offsetY: CGFloat
    let maskPadding: CGFloat
    
    public init(icon: String,
                iconColor: Color? = nil,
                iconSize: CGFloat,
                subIcon: String,
                subIconColor: Color? = nil,
                subIconSize: CGFloat,
                subOffsetX: CGFloat = 0,
                subOffsetY: CGFloat = 0,
                maskPadding: CGFloat = 0)
    {
        self.icon = icon
        self.iconColor = iconColor
        self.iconSize = iconSize
        self.subIcon = subIcon
        self.subIconColor = subIconColor
        self.subIconSize = subIconSize
        self.offsetX = subOffsetX
        self.offsetY = subOffsetY
        self.maskPadding = maskPadding
    }
    
    public var body: some View {
        ZStack(alignment: .bottomTrailing) {
            Image(systemName: icon)
                .resizable()
                .scaledToFit()
                .frame(width: iconSize, height: iconSize)
                .if(iconColor != nil) {
                    $0.foregroundStyle(iconColor!)
                }
                .maskReverse(alignment: .bottomTrailing) { HoleShapeMask() }
            
            Image(systemName: subIcon)
                .resizable()
                .scaledToFit()
                .if(iconColor != nil) {
                    $0.foregroundStyle(subIconColor!)
                }
                .frame(width: subIconSize, height: subIconSize)
                .frame(width: subIconSize + maskPadding, height: subIconSize + maskPadding)
                .offset(x: offsetX, y: offsetY)
        }
    }
}

@available(macOS 12.0, *)
fileprivate extension DoubleIcon {
    @ViewBuilder
    func HoleShapeMask() -> some View {
        Circle()
            .blendMode(.destinationOut)
            .frame(width: subIconSize + maskPadding, height: subIconSize + maskPadding)
            .offset(x: offsetX, y: offsetY)
    }
}
