import SwiftUI

public struct ProgressLine: View {
    @Binding var progressRatio: CGFloat
    let fillColor: Color
    
    public init (progressRatio: Binding<CGFloat>, fillColor: Color = Color.blue) {
        _progressRatio = progressRatio
        self.fillColor = fillColor
    }
    
    public var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .leading) {
                Rectangle().frame(width: geometry.size.width , height: geometry.size.height)
                    .opacity(0.3)
                    .foregroundColor(Color.gray)
                    .animation(nil, value: progressRatio)
                
                Rectangle().frame(width: min(progressRatio * geometry.size.width, geometry.size.width), height: geometry.size.height)
                    .foregroundColor( fillColor )
                    .animation(.linear, value: progressRatio)
            }.cornerRadius(45.0)
        }
    }
}
